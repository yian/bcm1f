# Developer: Alexander Ruede (CERN/KIT-IPE)
# email: aruede@cern.ch

# Python API for the BCM1F slow control.
# Further reading about the uHAL API for IPbus communication:
# https://ipbus.web.cern.ch/ipbus/doc/user/html/software/uhalQuickTutorial.html

import sys
import uhal
from time import sleep

class pyControl:

    def __init__( self, ipaddr ):
        uhal.setLogLevelTo( uhal.LogLevel.WARNING )
        self.ipaddr = ipaddr
        self.hw = uhal.getDevice( "GLIB" , "ipbusudp-2.0://" + self.ipaddr + ":50001", "file://./cfg/device_address_table_glib.xml" )

    # ---- IPbus ------------------------------------#

    def ipb_read( self, nodeID, dispatch=1 ):
        word = self.hw.getNode(nodeID).read()
        if dispatch == 1:
            self.hw.dispatch()
        return int(word)
    
    def ipb_write( self, nodeID, data, dispatch=1 ):
        self.hw.getNode(nodeID).write(data)
        if dispatch == 1:
            self.hw.dispatch()
        return int(data)

    def ipb_dispatch( self ):
        return self.hw.dispatch()

    # ---- Service ------------------------------------#

    def check_locked( self ):
        """
        Check the clock generator locked signal.
        """
        locked = self.hw.getNode("user.stat_regs.service.clk_gen_locked").read()
        self.hw.dispatch()
        return int(locked)

    # ---- FMC ---------------------------------------#

    def fmc_i2c_init( self, en=1, bus=0, presc=1000 ):
        """
        Initialize the I2C controller for the FMC
        """
        self.hw.getNode("user.ctrl_regs.fmc_i2c.enable").write(en)
        self.hw.getNode("user.ctrl_regs.fmc_i2c.bus_sel").write(bus)
        self.hw.getNode("user.ctrl_regs.fmc_i2c.prescaler").write(presc)
        self.hw.dispatch()

    def i2c_write( self, slv_data, slv_reg, slv_addr ):
        """
        Writing to the FMC I2C command register.
        Self-clearing strobe.
        """
        # Write register values
        self.hw.getNode("user.ctrl_regs.i2c_command.slv_data").write(slv_data)
        self.hw.getNode("user.ctrl_regs.i2c_command.slv_reg").write(slv_reg)
        self.hw.getNode("user.ctrl_regs.i2c_command.slv_addr").write(slv_addr)
        self.hw.getNode("user.ctrl_regs.i2c_command.write").write(1)
        self.hw.getNode("user.ctrl_regs.i2c_command.ralmode").write(0)
        self.hw.getNode("user.ctrl_regs.i2c_command.extmode").write(0)
        self.hw.getNode("user.ctrl_regs.i2c_command.strobe").write(1)
        self.hw.dispatch()
        # Read back values
        rb_data = self.hw.getNode("user.ctrl_regs.i2c_reply.slv_data").read()
        rb_reg = self.hw.getNode("user.ctrl_regs.i2c_reply.slv_reg").read()
        rb_addr = self.hw.getNode("user.ctrl_regs.i2c_reply.slv_addr").read()
        self.hw.dispatch()
        # Finish command
        self.hw.getNode("user.ctrl_regs.i2c_command.strobe").write(0)
        self.hw.dispatch()
        # Check readback
        # if not (rb_data==slv_data and rb_reg==slv_reg and rb_addr==slv_addr):
        #     raise ValueError( "FMC I2C command error!" )

    def fmc_i2c_config( self ):
        """
        Configuration procedure for SFP FMC.
        Finally closing i2c communication.
        """
        data = 0x4
        reg = 0x0
        addr = 0x74
        self.i2c_write( data, reg, addr)
        data = 0x0
        reg = 0x0
        addr = 0x38
        self.i2c_write( data, reg, addr)
        # disable i2c communication
        self.fmc_i2c_init( en=0, bus=0, presc=0)

    # ---- Slow Control --------------------------------#

    def reset( self ):
        self.hw.getNode("user.sc_regs.ctrl.reset").write(1)
        self.hw.getNode("user.ctrl_regs.fmc_i2c.reset").write(1)
        self.hw.dispatch()
        sleep(0.8)
        if not self.check_locked():
            raise ValueError("PLL not locked!!!")
        # else:
	    #     print("PLL locked.")
        self.hw.getNode("user.sc_regs.ctrl.reset").write(0)
        self.hw.getNode("user.ctrl_regs.fmc_i2c.reset").write(0)
        self.hw.getNode("user.sc_regs.ctrl.dispatch").write(0)
        self.hw.dispatch()

    ######################

    def write_hub_port( self, port=0b000 ):
        """
        Write the Hub and Port address.
        Hub address is hardcoded to 0b11111, as it is bonded on the BCM1F chips.
        Every 4th bit must be inverted and added to the word.
        hub_port: H4 + H3 + H2 + H1 + nH1 + H0 + P2 + P1 + P0 + nP0
        hub_port_extd is for the combined slow hub double addressing
        """
        if port > 7:
            raise ValueError("Port address cannot be > 7!")
        # Hub address
        hub_addr_bin = "111101"
        # hub_addr_bin = "000010"
        # Port address
        port_addr_bin = '{0:03b}'.format(port)
        nP0 = '0' if port_addr_bin[2] == '1' else '1'
        port_inv = port_addr_bin + nP0
        # Create word
        porthub = int( hub_addr_bin + port_inv, 2 )
        # Send
        self.hw.getNode("user.sc_regs.hub_port").write(porthub^0xfff)
        self.hw.dispatch()

    def write_hub_port_extd( self, port=0b000 ):
        """
        Write the Hub and Port address.
        Hub address is hardcoded to 0b11111, as it is bonded on the BCM1F chips.
        Every 4th bit must be inverted and added to the word.
        hub_port: H4 + H3 + H2 + H1 + nH1 + H0 + P2 + P1 + P0 + nP0
        hub_port_extd is for the combined slow hub double addressing
        """
        if port > 7:
            raise ValueError("Port address cannot be > 7!")
        # Hub address
        hub_addr_bin = "111101"
        # hub_addr_bin = "000010"
        # Port address
        port_addr_bin = '{0:03b}'.format(port)
        nP0 = '0' if port_addr_bin[2] == '1' else '1'
        port_inv = port_addr_bin + nP0
        # Create word
        porthub = int( hub_addr_bin + port_inv, 2 )
        # Send
        self.hw.getNode("user.sc_regs.hub_port_extd").write(porthub^0xfff)
        self.hw.dispatch()

    def write_slave( self, address, data, rw ):
        """
        Write to the slave.
        Every 4th bit must be inverted and added to the word.
        addr: S6 + S5 + S4 + S3 + nS3 + S2 + S1 + S0 + RW + nRW
        data: D7 + D6 + D5 + D4 + nD4 + D3 + D2 + D1 + D0 + nD0
        WARNING: Its does not have the inverted bits!
        """
        if address > 127:
            raise ValueError("Slave address cannot be > 127!")
        # elif data > 127:
        #     raise ValueError("Slave data cannot be > 127!")
        elif rw > 1:
            raise ValueError("RW must be either 1 or 0!")
        # RW
        rw_str = str(rw)
        nrw_str = '0' if rw == 1 else '1'
        # Slave address
        addr_bin = '{0:07b}'.format(address)
        nS3 = '0' if addr_bin[3] == '1' else '1'
        # addr = int( addr_bin[:4] + nS3 + addr_bin[4:7] + rw_str + nrw_str, 2 )
        addr = int( addr_bin[:4] + addr_bin[4:7] + rw_str + rw_str, 2 )
        # Slave data
        data_bin = '{0:08b}'.format(data)
        nD4 = '0' if data_bin[3] == '1' else '1'
        nD0 = '0' if data_bin[7] == '1' else '1'
        # data = int( data_bin[:4] + nD4 + data_bin[4:8] + nD0, 2 )
        data = int( data_bin[:4] + data_bin[4:8] + "0", 2 )
        # Send
        self.hw.getNode("user.sc_regs.address").write(addr^0xfff)
        self.hw.getNode("user.sc_regs.data").write(data^0xfff)
        self.hw.dispatch()


    def write_command( self, port, port_extd, address, data, extd):
        rw = 0
        self.hw.getNode("user.sc_regs.ctrl.extended").write(extd)
        ready_flg = self.hw.getNode("user.sc_regs.ready").read()
        self.hw.dispatch()
        cnt = 0
        while int(ready_flg&0xf) == 0:
            if cnt > 31:
                raise TimeoutError("Slow control transaction timeout!")
            ready_flg = self.hw.getNode("user.sc_regs.ready").read()
            self.hw.dispatch()
            cnt += 1
        ##
        self.write_hub_port( port=port )
        self.write_hub_port_extd( port=port_extd )
        self.write_slave( address=address, data=data, rw=rw )
        self.hw.dispatch()
        self.hw.getNode("user.sc_regs.ctrl.dispatch").write(1)
        self.hw.dispatch()
        self.hw.getNode("user.sc_regs.ctrl.dispatch").write(0)
        self.hw.dispatch()
