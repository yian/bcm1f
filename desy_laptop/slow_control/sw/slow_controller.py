# Developer: Alexander Ruede (CERN/KIT-IPE)
# email: aruede@cern.ch

# BCM1F Slow Control

import sys
from pyControl import *
from time import sleep

def usage():
    print 'Usage:', sys.argv[0], ' <IP addr> <slowhub version> <port> <laser> <bias> <gain>'
    print '<IP addr>: IP address of slow control GLIB'
    print '<slowhub version> is one of: old, new'
    print '<port> selects the AOH number: 0-7'
    print '<laser> selects the AOH laser: L, M, R, all'
    print '<bias> selects laser bias value: 0-127'
    print '<gain> selects AOH gain: 0-3, only one gain for ALL lasers selectable!'
    sys.exit( -1 )

if len( sys.argv ) < 7 or '-h' in sys.argv or '--help' in sys.argv:
    usage()
# IP address
ip_addr = sys.argv[1]
# Slow hub Veresion
if sys.argv[2] == "old":
    extended = 0
elif sys.argv[2] == "new":
    extended = 1
else:
    usage()
    raise ValueError("arg[2]: Specify if 'old' or 'new' slow hub chip is used!")
# Port number
if extended == 0:
    port = int(sys.argv[3])
    port_extd = port
elif extended == 1:
    port = 0
    port_extd = int(sys.argv[3])
else:
    port = 0
    port_extd = int(sys.argv[3])
# Laser position
all_lasers = False
if sys.argv[4] == "l" or sys.argv[4] == "L":
    address = 0x60
elif sys.argv[4] == "m" or sys.argv[4] == "M":
    address = 0x61
elif sys.argv[4] == "r" or sys.argv[4] == "R":
    address = 0x62
elif sys.argv[4] == "all":
    all_lasers = True
else:
    usage()
    raise ValueError("arg[4]: Specify laser position: L/M/R")
# Bias value
bias = int(sys.argv[5])
# Gain
if sys.argv[6] == "0":
    gain = 0
elif sys.argv[6] == "1":
    gain = 21
elif sys.argv[6] == "2":
    gain = 42
elif sys.argv[6] == "3":
    gain = 63
else:
    usage()
    raise ValueError("arg[6]: Select gain (one for all lasers): 0-3 ")

################################################
# Create object, specifying the IP address
# and connecting to the hardware IP endpoint
################################################
sc = pyControl( ip_addr )
sleep(0.2)
# Reset the firmware
sc.reset()
# Conifgure FMC
sc.fmc_i2c_init()
sc.fmc_i2c_config()

print "--> Configure AOH with " + sys.argv[2] + " slow hub version."
if extended:
    print "Port: " + str(port_extd) + ", Laser: " + sys.argv[4]
else:
    print "Port: " + str(port) + ", Laser: " + sys.argv[4]
print "Bias: " + str(bias) + ", Gain: " + sys.argv[6]

if not all_lasers:
    sc.write_command( port=port, port_extd=port_extd, address=address, data=bias, extd=extended )
    sc.write_command( port=port, port_extd=port_extd, address=0x63, data=gain, extd=extended )
else:
    sc.write_command( port=port, port_extd=port_extd, address=0x60, data=bias, extd=extended )
    sc.write_command( port=port, port_extd=port_extd, address=0x61, data=bias, extd=extended )
    sc.write_command( port=port, port_extd=port_extd, address=0x62, data=bias, extd=extended )
    sc.write_command( port=port, port_extd=port_extd, address=0x63, data=gain, extd=extended )

print "DONE"
