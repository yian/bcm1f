# Description

New slow control for BCM1F. Compatible with the new and old slow hub chip.

The firmware image can be found in `bcm1f_sw/slow_control/fw/bin`.

The software is found under `bcm1f_sw/slow_control/sw`.
Use the `env.sh` script to set the environemt (its the same as for ubcm).

The file `pyControl.py` is the API for users to write a slow control application.
You can use `slow_controller.py` as an example and/or to control a single AOH.


Example for `slow_controller.py`:

```
cd bcm1f_sw/slow_control/sw
. env.sh
python slow_controller.py -h
python slow_controller.py 128.141.58.146 new 1 R 30 3
```