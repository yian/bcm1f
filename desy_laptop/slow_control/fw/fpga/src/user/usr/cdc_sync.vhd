--==================================== Module Information =========================================--
-- Engineer         : Alexander Ruede (CERN/KIT-IPE)  
-- Description      : Synchonize asynchronous in/outputs such as reset signals with clk
-- Comment          : Has to be used with following contraint:
--                    set_false_path -to [get_pins -hier *s_level_out_cdc_to*/D]
--                    No reset implemented yet, beware!
--=================================================================================================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity cdc_sync is
  Generic(
    n_stages : integer := 1;
    mode : integer range 0 to 1 := 0; -- 0: std_logic, 1: vector
    vec_size : integer := 1;
    init_val : std_logic := '1'
  );
  Port (
    clk : in std_logic;
    primary_in : in std_logic;
    primary_in_vec : in std_logic_vector( 0 to vec_size-1 );
    secondary_out : out std_logic;
    secondary_out_vec : out std_logic_vector( 0 to vec_size-1 )
  );
end cdc_sync;

architecture Behavioral of cdc_sync is

  attribute ASYNC_REG : string;
  attribute shreg_extract : string;

  signal s_level_out_cdc_to : std_logic := init_val;
  attribute ASYNC_REG of s_level_out_cdc_to : signal is "TRUE";
  attribute shreg_extract of s_level_out_cdc_to : signal is "no";
  signal s_level_out_cdc_to_vec : std_logic_vector( 0 to vec_size-1 ) := ( others => init_val );
  attribute ASYNC_REG of s_level_out_cdc_to_vec : signal is "TRUE";
  attribute shreg_extract of s_level_out_cdc_to_vec : signal is "no";

  signal s_level_out : std_logic_vector( n_stages-1 downto 0 ) := ( others => init_val );
  attribute ASYNC_REG of s_level_out : signal is "TRUE";
  attribute shreg_extract of s_level_out : signal is "no";
  type level_out_vec_t is array ( natural range <> ) of std_logic_vector( 0 to vec_size-1 );
  signal s_level_out_vec : level_out_vec_t( n_stages-1 downto 0 ):= ( others => ( others => init_val ) );
  attribute ASYNC_REG of s_level_out_vec : signal is "TRUE";
  attribute shreg_extract of s_level_out_vec : signal is "no";

  signal secondary_out_s : std_logic := init_val;

------------------------------------------------------------------------------------------
begin
------------------------------------------------------------------------------------------

----------------------------------
    std_logic_sync : if mode = 0 generate
    synchronize : process( clk )
    begin
      if rising_edge( clk ) then
        if primary_in = '1' then
          s_level_out_cdc_to <= init_val;
          s_level_out <= ( others => init_val );
          secondary_out_s <= init_val;
        else
          s_level_out_cdc_to <= primary_in;
          s_level_out( 0 ) <= s_level_out_cdc_to;
          for i in 1 to n_stages-1 loop
            s_level_out( i ) <= s_level_out( i-1 );
          end loop;
          secondary_out_s <= s_level_out( n_stages-1 );
        end if;
      end if;
    end process;
  end generate;

  secondary_out <= secondary_out_s;
----------------------------------

  -- std_logic_sync : if mode = 0 generate
  --   synchronize : process( clk )
  --   begin
  --     if rising_edge( clk ) then
  --       s_level_out_cdc_to <= primary_in;
  --       s_level_out( 0 ) <= s_level_out_cdc_to;
  --       for i in 1 to n_stages-1 loop
  --         s_level_out( i ) <= s_level_out( i-1 );
  --       end loop;
  --       secondary_out <= s_level_out( n_stages-1 );
  --     end if;
  --   end process;
  -- end generate;

  vector_sync : if mode = 1 generate
    synchronize_vec : process( clk )
    begin
      if rising_edge( clk ) then
        s_level_out_cdc_to_vec <= primary_in_vec;
        s_level_out_vec( 0 ) <= s_level_out_cdc_to_vec;
        for i in 1 to n_stages-1 loop
          s_level_out_vec( i ) <= s_level_out_vec( i-1 );
        end loop;
        secondary_out_vec <= s_level_out_vec( n_stages-1 );
      end if;
    end process;
  end generate;

end Behavioral;
