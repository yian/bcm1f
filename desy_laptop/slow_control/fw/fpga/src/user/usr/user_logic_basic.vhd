library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
--! xilinx packages
library unisim;
use unisim.vcomponents.all;
--! system packages
use work.system_flash_sram_package.all;
use work.system_pcie_package.all;
use work.system_package.all;
use work.fmc_package.all;
use work.wb_package.all;
use work.ipbus.all;
--! user packages
use work.user_package.all;
use work.user_version_package.all;

entity user_logic is
port
(
	--================================--
	-- USER MGT REFCLKs
	--================================--
   -- BANK_112(Q0):  
   clk125_1_p	                        : in	  std_logic;  		    
   clk125_1_n	                        : in	  std_logic;  		  
   cdce_out0_p	                        : in	  std_logic;  		  
   cdce_out0_n	                        : in	  std_logic; 		  
   -- BANK_113(Q1):                 
   fmc2_clk0_m2c_xpoint2_p	            : in	  std_logic;
   fmc2_clk0_m2c_xpoint2_n	            : in	  std_logic;
   cdce_out1_p	                        : in	  std_logic;       
   cdce_out1_n	                        : in	  std_logic;         
   -- BANK_114(Q2):                 
   pcie_clk_p	                        : in	  std_logic; 			  
   pcie_clk_n	                        : in	  std_logic;			  
   cdce_out2_p  	                     : in	  std_logic;			  
   cdce_out2_n  	                     : in	  std_logic;			  
   -- BANK_115(Q3):                 
   clk125_2_i                          : in	  std_logic;		      
   fmc1_gbtclk1_m2c_p	               : in	  std_logic;     
   fmc1_gbtclk1_m2c_n	               : in	  std_logic;     
   -- BANK_116(Q4):                 
   fmc1_gbtclk0_m2c_p	               : in	  std_logic;	  
   fmc1_gbtclk0_m2c_n	               : in	  std_logic;	  
   cdce_out3_p	                        : in	  std_logic;		  
   cdce_out3_n	                        : in	  std_logic;		    
   --================================--
	-- USER FABRIC CLOCKS
	--================================--
   xpoint1_clk3_p	                     : in	  std_logic;		   
   xpoint1_clk3_n	                     : in	  std_logic;		   
   ------------------------------------  
   cdce_out4_p                         : in	  std_logic;                
   cdce_out4_n                         : in	  std_logic;              
   ------------------------------------
   amc_tclkb_o					            : out	  std_logic;
   ------------------------------------      
   fmc1_clk0_m2c_xpoint2_p	            : in	  std_logic;
   fmc1_clk0_m2c_xpoint2_n	            : in	  std_logic;
   fmc1_clk1_m2c_p		               : in	  std_logic;	
   fmc1_clk1_m2c_n		               : in	  std_logic;	
   fmc1_clk2_bidir_p		               : in	  std_logic;	
   fmc1_clk2_bidir_n		               : in	  std_logic;	
   fmc1_clk3_bidir_p		               : in	  std_logic;	
   fmc1_clk3_bidir_n		               : in	  std_logic;	
   ------------------------------------
   fmc2_clk1_m2c_p	                  : in	  std_logic;		
   fmc2_clk1_m2c_n	                  : in	  std_logic;		
	--================================--
	-- GBT PHASE MONITORING MGT REFCLK
	--================================--
   cdce_out0_gtxe1_o                   : out   std_logic;  		  
   cdce_out3_gtxe1_o                   : out   std_logic;  
	--================================--
	-- AMC PORTS
	--================================--
   amc_port_tx_p				            : out	  std_logic_vector(1 to 15);
	amc_port_tx_n				            : out	  std_logic_vector(1 to 15);
	amc_port_rx_p				            : in	  std_logic_vector(1 to 15);
	amc_port_rx_n				            : in	  std_logic_vector(1 to 15);
	------------------------------------
	amc_port_tx_out			            : out	  std_logic_vector(17 to 20);	
	amc_port_tx_in				            : in	  std_logic_vector(17 to 20);		
	amc_port_tx_de				            : out	  std_logic_vector(17 to 20);	
	amc_port_rx_out			            : out	  std_logic_vector(17 to 20);	
	amc_port_rx_in				            : in	  std_logic_vector(17 to 20);	
	amc_port_rx_de				            : out	  std_logic_vector(17 to 20);	
	--================================--
	-- SFP QUAD
	--================================--
	sfp_tx_p						            : out	  std_logic_vector(1 to 4);
	sfp_tx_n						            : out	  std_logic_vector(1 to 4);
	sfp_rx_p						            : in	  std_logic_vector(1 to 4);
	sfp_rx_n						            : in	  std_logic_vector(1 to 4);
	sfp_mod_abs					            : in	  std_logic_vector(1 to 4);		
	sfp_rxlos					            : in	  std_logic_vector(1 to 4);		
	sfp_txfault					            : in	  std_logic_vector(1 to 4);				
	--================================--
	-- FMC1
	--================================--
	fmc1_tx_p					            : out	  std_logic_vector(1 to 4);
	fmc1_tx_n                           : out	  std_logic_vector(1 to 4);
	fmc1_rx_p                           : in	  std_logic_vector(1 to 4);
	fmc1_rx_n                           : in	  std_logic_vector(1 to 4);
	------------------------------------
	fmc1_io_pin					            : inout fmc_io_pin_type;
	------------------------------------
	fmc1_clk_c2m_p				            : out	  std_logic_vector(0 to 1);
	fmc1_clk_c2m_n				            : out	  std_logic_vector(0 to 1);
	fmc1_present_l				            : in	  std_logic;
	--================================--
	-- FMC2
	--================================--
	fmc2_io_pin					            : inout fmc_io_pin_type;
	------------------------------------
	fmc2_clk_c2m_p				            : out	  std_logic_vector(0 to 1);
	fmc2_clk_c2m_n				            : out	  std_logic_vector(0 to 1);
	fmc2_present_l				            : in	  std_logic;
   --================================--      
	-- SYSTEM GBE   
	--================================--      
   sys_eth_amc_p1_tx_p		            : in	  std_logic;	
   sys_eth_amc_p1_tx_n		            : in	  std_logic;	
   sys_eth_amc_p1_rx_p		            : out	  std_logic;	
   sys_eth_amc_p1_rx_n		            : out	  std_logic;	
	------------------------------------
	user_mac_syncacqstatus_i            : in	  std_logic_vector(0 to 3);
	user_mac_serdes_locked_i            : in	  std_logic_vector(0 to 3);
	--================================--   										
	-- SYSTEM PCIe				   												
	--================================--   
   sys_pcie_mgt_refclk_o	            : out	  std_logic;	  
   user_sys_pcie_dma_clk_i             : in	  std_logic;	  
   ------------------------------------
	sys_pcie_amc_tx_p		               : in	  std_logic_vector(0 to 3);    
   sys_pcie_amc_tx_n		               : in	  std_logic_vector(0 to 3);    
   sys_pcie_amc_rx_p		               : out	  std_logic_vector(0 to 3);    
   sys_pcie_amc_rx_n		               : out	  std_logic_vector(0 to 3);    
   ------------------------------------
	user_sys_pcie_slv_o	               : out   R_slv_to_ezdma2;									   	
	user_sys_pcie_slv_i	               : in    R_slv_from_ezdma2; 	   						    
	user_sys_pcie_dma_o                 : out   R_userDma_to_ezdma2_array  (1 to 7);		   					
	user_sys_pcie_dma_i                 : in 	  R_userDma_from_ezdma2_array(1 to 7);		   	
	user_sys_pcie_int_o 	               : out   R_int_to_ezdma2;									   	
	user_sys_pcie_int_i 	               : in    R_int_from_ezdma2; 								    
	user_sys_pcie_cfg_i 	               : in	  R_cfg_from_ezdma2; 								   	
	--================================--
	-- SRAMs
	--================================--
	user_sram_control_o		            : out	  userSramControlR_array(1 to 2);
	user_sram_addr_o			            : out	  array_2x21bit;
	user_sram_wdata_o			            : out	  array_2x36bit;
	user_sram_rdata_i			            : in 	  array_2x36bit;
	------------------------------------
   sram1_bwa                           : out	  std_logic;  
   sram1_bwb                           : out	  std_logic;  
   sram1_bwc                           : out	  std_logic;  
   sram1_bwd                           : out	  std_logic;  
   sram2_bwa                           : out	  std_logic;  
   sram2_bwb                           : out	  std_logic;  
   sram2_bwc                           : out	  std_logic;  
   sram2_bwd                           : out	  std_logic;    
   --================================--               
	-- CLK CIRCUITRY              
	--================================--    
   fpga_clkout_o	  			            : out	  std_logic;	
   ------------------------------------
   sec_clk_o		                     : out	  std_logic;	
	------------------------------------
	user_cdce_locked_i			         : in	  std_logic;
	user_cdce_sync_done_i					: in	  std_logic;
	user_cdce_sel_o			            : out	  std_logic;
	user_cdce_sync_o			            : out	  std_logic;
	--================================--  
	-- USER BUS  
	--================================--       
	wb_miso_o				               : out	  wb_miso_bus_array(0 to number_of_wb_slaves-1);
	wb_mosi_i				               : in 	  wb_mosi_bus_array(0 to number_of_wb_slaves-1);
	------------------------------------
	ipb_clk_i				               : in 	  std_logic;
	ipb_miso_o			                  : out	  ipb_rbus_array(0 to number_of_ipb_slaves-1);
	ipb_mosi_i			                  : in 	  ipb_wbus_array(0 to number_of_ipb_slaves-1);   
   --================================--
	-- VARIOUS
	--================================--
   reset_i						            : in	  std_logic;	    
	user_clk125_i                  		: in	  std_logic;       
   user_clk200_i                  		: in	  std_logic;       
   ------------------------------------   
   sn			                           : in    std_logic_vector(7 downto 0);	   
   ------------------------------------   
   amc_slot_i									: in    std_logic_vector( 3 downto 0);
	mac_addr_o 					            : out   std_logic_vector(47 downto 0);
   ip_addr_o					            : out   std_logic_vector(31 downto 0);
   ------------------------------------	
   user_v6_led_o                       : out	  std_logic_vector(1 to 2)
);                         	
end user_logic;
							
architecture user_logic_arch of user_logic is                    	


	signal ctrl_reg		               : array_32x32bit;
	signal stat_reg		               : array_32x32bit;

	signal led1									: std_logic;
	signal led2									: std_logic;

	signal test_word							: std_logic_vector(31 downto 0);
	
	-- clock generator --
	signal clk_gen_locked : std_logic := '0';
	signal clk_40 : std_logic;
	signal clk_40_90deg : std_logic;

	-- command fifo --
	signal fifo_empty : std_logic;
	signal fifo_full : std_logic;
	signal fifo_data : std_logic_vector( 22 downto 0 );
	signal rd_en : std_logic;

	signal ser_data : std_logic;

	-- fmc1/fmc2 i2c --
	signal fmc1_p : std_logic;
	signal fmc2_p : std_logic;
	signal fmc1_n : std_logic;
	signal fmc2_n : std_logic;
	signal i2c_enable : std_logic := '0';
	signal i2c_bus_select : std_logic_vector( 2 downto 0):= "000";
	signal i2c_prescaler : std_logic_vector( 9 downto 0);
	signal i2c_command : std_logic_vector(31 downto 0);
	signal i2c_reply : std_logic_vector(31 downto 0);
	signal sfp_data_o : std_logic;
	signal sfp_clk_o : std_logic;

-----------------------
begin-- ARCHITECTURE
-----------------------                              
 
   
   --#############################--
   --## GLIB IP & MAC ADDRESSES ##--
   --#############################--
   
	ip_rarp: if force_rarp = true generate
     ip_addr_o				               <= x"00000000";
	end generate ip_rarp;
	
	ip_user: if force_rarp = false generate 
	  ip_addr_o				               <= x"c0a800a"     & amc_slot_i;  -- 192.168.0.[160:175]
   end generate ip_user;
	
	-- default mac addr when eeprom with invalid contents
	mac_addr_o 				               <= x"080030F100a" & amc_slot_i;  -- 08:00:30:F1:00:0[A0:AF] 
  
	--===========================================--
	stat_regs_inst: entity work.ipb_user_status_regs
	--===========================================--
	port map
	(
		clk					=> ipb_clk_i,
		reset				=> reset_i,
		ipb_mosi_i			=> ipb_mosi_i(user_ipb_stat_regs),
		ipb_miso_o			=> ipb_miso_o(user_ipb_stat_regs),
		regs_i				=> stat_reg
	);


	--===========================================--
	ctrl_regs_inst: entity work.ipb_user_control_regs
	--===========================================--
	port map
	(
		clk					=> ipb_clk_i,
		reset				=> reset_i,
		ipb_mosi_i			=> ipb_mosi_i(user_ipb_ctrl_regs),
		ipb_miso_o			=> ipb_miso_o(user_ipb_ctrl_regs),
		regs_o				=> ctrl_reg
	);

   --===========================================--
	-- register mapping
	--===========================================--
	led1 			<= ctrl_reg(0)(0);
	led2 			<= ctrl_reg(0)(4);
	--
	stat_reg(0)	<= x"67_6f_6c_64"; -- 'g' 'o' 'l' 'd'
	stat_reg(1)	<=  std_logic_vector(to_unsigned(usr_ver_major,4)) &
					std_logic_vector(to_unsigned(usr_ver_minor,4)) &
					std_logic_vector(to_unsigned(usr_ver_build,8)) &
					std_logic_vector(to_unsigned(usr_ver_year ,7)) &
					std_logic_vector(to_unsigned(usr_ver_month,4)) &
					std_logic_vector(to_unsigned(usr_ver_day  ,5)) ;

	--===========================================--
	-- I/O mapping
	--===========================================--
	user_v6_led_o(1) <= led1;
	user_v6_led_o(2) <= led2;
	
	-- Clock generator
	clk_generator : entity work.clk_gen
	port map(
		usr_clk => user_clk125_i,
		rst => reset_i,
		locked => clk_gen_locked,
		clk_40 => clk_40,
		clk_40_90deg => clk_40_90deg
	);

	stat_reg(2)(0) <= clk_gen_locked;
	
	
	slow_controller : entity work.slow_control
	port map(
		ipb_clk => ipb_clk_i,
		ipb_rst => reset_i,
		ipb_mosi_i => ipb_mosi_i( user_ipb_sc_regs ),
		ipb_miso_o => ipb_miso_o( user_ipb_sc_regs ),
		clk_90deg => clk_40_90deg,
		ser_data => ser_data,
		hub_port_o => stat_reg(4)(9 downto 0),
		hub_port_extd_o => stat_reg(5)(9 downto 0),
		addr_o => stat_reg(6)(9 downto 0),
		data_o => stat_reg(7)(9 downto 0),
		disp_o => stat_reg(8)(0)
	);
	

	-----------------------------------------------
    -- FMC 
	-----------------------------------------------
	
	sfp_clk_o <= clk_40;
	sfp_data_o <= ser_data;
	
	--========SFP A=========
	--TX
	OBUFDS_inst_A : OBUFDS 
	generic map (IOSTANDARD => "DEFAULT") 
	port map (
		O => fmc1_io_pin.la_p(2),	
		OB => fmc1_io_pin.la_n(2),
		I => sfp_data_o
	);
	--RX
	--test_rx(0) <= fmc1_io_pin.la_p(1) and not fmc1_io_pin.la_n(1);
	--========SFP B=========
	--TX
	OBUFDS_inst_B : OBUFDS 
	generic map (IOSTANDARD => "DEFAULT") 
	port map (
		O => fmc1_io_pin.la_p(4),	
		OB => fmc1_io_pin.la_n(4),
		I => sfp_clk_o
	);
	--RX
	--test_rx(1) <= fmc1_io_pin.la_p(3) and not fmc1_io_pin.la_n(3);
	--========SFP C=========
	--TX
	OBUFDS_inst_C : OBUFDS 
	generic map (IOSTANDARD => "DEFAULT") 
	port map (
	--!!!!!!!!!!!!!INVERTED LINES!!!!!!!!!!!!!!!!!!!!
		O => fmc1_io_pin.la_p(6),	
		OB => fmc1_io_pin.la_n(6),
		I => sfp_data_o		
	);
	--RX
	--test_rx(2) <= fmc1_io_pin.la_p(5) and not fmc1_io_pin.la_n(5);
	--========SFP D=========
	--TX
	OBUFDS_inst_D : OBUFDS 
	generic map (IOSTANDARD => "DEFAULT") 
	port map (
		O => fmc1_io_pin.la_p(8),	
		OB => fmc1_io_pin.la_n(8),
		I => sfp_clk_o
	);
	--RX
	--test_rx(3) <= fmc1_io_pin.la_p(7) and not fmc1_io_pin.la_n(7);
	--========SFP E=========
	--TX
	OBUFDS_inst_E : OBUFDS 
	generic map (IOSTANDARD => "DEFAULT") 
	port map (
		O => fmc1_io_pin.la_p(10),	
		OB => fmc1_io_pin.la_n(10),
		I =>  sfp_data_o
	);
	--RX
	--test_rx(4) <= fmc1_io_pin.la_p(9) and not fmc1_io_pin.la_n(9);
	--========SFP F=========
	--TX
	OBUFDS_inst_F : OBUFDS 
	generic map (IOSTANDARD => "DEFAULT") 
	port map (
		O => fmc1_io_pin.la_p(12),	
		OB => fmc1_io_pin.la_n(12),
		I => sfp_clk_o
	);
	--RX
	--test_rx(5) <= fmc1_io_pin.la_p(11) and not fmc1_io_pin.la_n(11);
	--========SFP G=========
	--TX
	OBUFDS_inst_G : OBUFDS 
	generic map (IOSTANDARD => "DEFAULT") 
	port map (
		O => fmc1_io_pin.la_p(14),	
		OB => fmc1_io_pin.la_n(14),
		I => sfp_data_o
	);
	--RX
	--test_rx(6) <= fmc1_io_pin.la_p(13) and not fmc1_io_pin.la_n(13);
	--========SFP H=========
	--TX
	OBUFDS_inst_H : OBUFDS 
	generic map (IOSTANDARD => "DEFAULT") 
	port map (
		O => fmc1_io_pin.la_p(16),	
		OB => fmc1_io_pin.la_n(16),
		I => sfp_clk_o
	);
	--RX
	--test_rx(7) <= fmc1_io_pin.la_p(15) and not fmc1_io_pin.la_n(15);
	
	------------------------------
	--- i2c controller for FMC ---
	------------------------------

	i2c_enable <= ctrl_reg(1)(0);
	i2c_bus_select <= ctrl_reg(1)(3 downto 1);
	i2c_prescaler <= ctrl_reg(1)(13 downto 4);
	i2c_command <= ctrl_reg(2);
	i2c_reply <= ctrl_reg(3);

	fmc_i2c: entity work.i2c_master_dual
	port map
	(
		reset => reset_i or ctrl_reg(1)(31),
		clk => ipb_clk_i,
		enable => i2c_enable,
		bus_select => i2c_bus_select,
		prescaler => i2c_prescaler,
		command => i2c_command,
		reply => i2c_reply,
		scl_io(0) => fmc1_p, 	
		scl_io(1) => fmc2_p, 
		sda_io(0) => fmc1_n, 	
		sda_io(1) => fmc2_n
	);

	fmc1_io_pin.la_p(32) <= fmc1_p;
	fmc2_io_pin.la_p(32) <= fmc2_p;
	fmc1_io_pin.la_n(32) <= fmc1_n;
	fmc2_io_pin.la_n(32) <= fmc2_n;

end user_logic_arch;