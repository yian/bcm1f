library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;

entity slow_control is
port
(
	ipb_clk : in std_logic;
	ipb_rst : in std_logic;
	ipb_mosi_i : in ipb_wbus;
	ipb_miso_o : out ipb_rbus;
	--
	clk_90deg : in std_logic;
	ser_data : out std_logic;
	hub_port_o : out std_logic_vector( 9 downto 0 );
	hub_port_extd_o : out std_logic_vector( 9 downto 0 );
	addr_o : out std_logic_vector( 9 downto 0 );
	data_o : out std_logic_vector( 9 downto 0 );
	disp_o : out std_logic
);
end slow_control;

architecture Behavioral of slow_control is

	-- IPb slave
	constant stat_addr_width : natural := 1;
	signal sc_status : std_logic_vector(2 ** stat_addr_width * 32 - 1 downto 0);
	constant ctrl_addr_width : natural := 3;
	signal sc_control : std_logic_vector(2 ** ctrl_addr_width * 32 - 1 downto 0);
	signal stb : std_logic_vector(2 ** ctrl_addr_width - 1 downto 0);

	-- Slow Control Signals
	signal rst : std_logic;
	signal hub_port : std_logic_vector( 9 downto 0 );
	signal hub_port_extd : std_logic_vector( 9 downto 0 );
	signal addr : std_logic_vector( 9 downto 0 );
	signal data : std_logic_vector( 9 downto 0 );
	signal extd : std_logic;
	signal disp : std_logic;
	signal ready : std_logic;
	
begin

	ipb_synced_sc_slave : entity work.ipbus_syncreg
	generic map(
		ctrl_addr_width => ctrl_addr_width,
		stat_addr_width => stat_addr_width
	)
	port map(
		clk => ipb_clk,
		rst => ipb_rst,
		ipb_in => ipb_mosi_i,
		ipb_out => ipb_miso_o,
		slv_clk => clk_90deg,
		d => sc_status,
		q => sc_control,
		stb => stb
	);

	-- IPbus controlled serializer

	rst <= sc_control(0); -- (0)(0)
	extd <= sc_control(1); -- (0)(1)
	disp <= sc_control(2); -- (0)(2)
	hub_port <= sc_control( (32*1)+9 downto 32*1 ); -- (1)(9 downto 0)
	hub_port_extd <= sc_control( (32*2)+9 downto 32*2 ); -- (2)(9 downto 0)
	addr <= sc_control( (32*3)+9 downto 32*3 ); -- (3)(9 downto 0)
	data <= sc_control( (32*4)+9 downto 32*4 ); --(4)(9 downto 0)
	--
	sc_status(0) <= ready;
	sc_status(31 downto 22) <= "1010111100";

	--serializer : entity work.slow_test
	serializer : entity work.slow_commands
	port map(
		clk => '0',
		clk_90deg => clk_90deg,
		reset => rst,
		hub_port => hub_port,
		hub_port_extd => hub_port_extd,
		addr => addr,
		data => data,
		extd => extd,
		disp => disp,
		ready => ready,
		ser_data => ser_data
	);
	
	hub_port_o <= hub_port;
	hub_port_extd_o <= hub_port_extd;
	addr_o <= addr;
	data_o <= data;
	disp_o <= disp;

end Behavioral;

