LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY slow_commands_tb IS
END slow_commands_tb;
 
ARCHITECTURE behavior OF slow_commands_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT slow_commands
    PORT(
		clk : in std_logic;
         clk_90deg : IN  std_logic;
         reset : IN  std_logic;
         hub_port : IN  std_logic_vector(9 downto 0);
         addr : IN  std_logic_vector(9 downto 0);
         data : IN  std_logic_vector(9 downto 0);
         disp : IN  std_logic;
		hub_port_extd : in std_logic_vector( 9 downto 0 );
		extd : in std_logic;
		ready : out std_logic;
		ser_data : out std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clk_90deg : std_logic := '0';
   signal reset : std_logic := '0';
   signal hub_port : std_logic_vector(9 downto 0) := (others => '0');
	signal hub_port_extd : std_logic_vector( 9 downto 0 );
   signal addr : std_logic_vector(9 downto 0) := (others => '0');
   signal data : std_logic_vector(9 downto 0) := (others => '0');
   signal disp : std_logic := '0';
	signal extd : std_logic;
	
	signal locked : std_logic;
	signal clk_125 : std_logic := '1';
	
	signal ser_data : std_logic;
	signal ready : std_logic;

BEGIN

clk125_p : process begin  wait for 62.5 ns; clk_125 <= not clk_125; end process;

clk_generator : entity work.clk_gen
	port map(
		usr_clk => clk_125,
		rst => reset,
		locked => locked,
		clk_40 => clk,
		clk_40_90deg => clk_90deg
	);
 
	-- Instantiate the Unit Under Test (UUT)
   uut: slow_commands PORT MAP (
	   clk => '0',
          clk_90deg => clk_90deg,
          reset => reset,
          hub_port => hub_port,
          addr => addr,
          data => data,
          disp => disp,
			 ser_data => ser_data,
			 hub_port_extd => hub_port_extd,
			 extd => extd,
			 ready => ready
        );



   -- Stimulus process
   stim_proc: process
   begin
		reset <= '1';
		wait for 100 ns;
		reset <= '0';
      hub_port <= "1010101000";
		addr <= "1011011100";
		data <= "1101001110";
		extd <= '0';
		wait until locked = '1';
		wait for 100 ns;
		wait until rising_edge( clk );
		disp <= '1';
		wait for 1000 ns;
		wait until rising_edge( clk );
		wait until rising_edge( clk );
		wait until rising_edge( clk );
		wait until rising_edge( clk );
		wait until rising_edge( clk );
		wait until rising_edge( clk );
		disp <= '0';
		wait until ready = '1';
		wait until rising_edge( clk );
		wait until rising_edge( clk );
		wait until rising_edge( clk );
		wait until rising_edge( clk );
		wait until rising_edge( clk );
		hub_port <= "1010101011";
		addr <= "1111011111";
		data <= "0011001101";
		hub_port_extd <= "0011111111";
		extd <= '1';
		wait until rising_edge( clk );
		disp <= '1';
		wait until rising_edge( clk );
		wait until rising_edge( clk );
		wait until rising_edge( clk );
		wait until rising_edge( clk );
		wait until rising_edge( clk );wait until rising_edge( clk );
		disp <= '0';
      wait;
   end process;

END;
