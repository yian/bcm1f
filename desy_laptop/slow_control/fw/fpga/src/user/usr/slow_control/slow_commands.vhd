library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- sorry for this super long state machine...

-- working version, tested!

entity slow_commands is
port
(
	clk : in std_logic;
	clk_90deg : in std_logic;
	reset : in std_logic;
	hub_port : in std_logic_vector( 9 downto 0 );
	hub_port_extd  : in std_logic_vector( 9 downto 0 );
	extd : in std_logic;
	addr : in std_logic_vector( 9 downto 0 );
	data : in std_logic_vector( 9 downto 0 );
	disp : in std_logic;
	ready : out std_logic;
	ser_data : out std_logic
);
end slow_commands;

architecture Behavioral of slow_commands is

	type state_t is ( idle, start_hub_port, write_hub_port, start_addr, write_addr, write_data, start_hub_port_extd, write_hub_port_extd, ack, stopp );
	signal state : state_t := idle;
	signal state_prev : state_t;
	
	signal outputs : std_logic;
	signal start_flg : std_logic;
	
	type cnt_t is ( one, two, zero );
	signal d_cnt : cnt_t;

	signal latch : std_logic;
	signal slow_cnt : natural := 0;
	signal slow_clk : std_logic;

	constant slow_cnt_max : natural := 64;
	signal counter : natural;
	constant counter_max : natural := slow_cnt_max;
	signal double_clk : std_logic;
	
	signal disp_selfc : std_logic;
	signal high_flag : std_logic;

begin

	self_clear_p : process( clk_90deg )
    begin
        if rising_edge( clk_90deg ) then
            if disp = '1' then
                high_flag <= '1';
            else
                high_flag <= '0';
            end if;
            if high_flag = '1' then
                disp_selfc <= '0';
            else
                disp_selfc <= disp;
            end if;
        end if;
    end process;

	state_machine_p : process( clk_90deg )
		variable cnt : integer := 10;
	begin
		if falling_edge( clk_90deg ) then
			if reset = '1' then
				state <= idle;
			else
				case state is
					when idle => 
						--if disp = '1' then
						if disp_selfc = '1' then
							state <= start_hub_port;
							ready <= '0';
						else
							state <= idle;
							ready <= '1';
						end if;
					when start_hub_port => 
						state <= write_hub_port;
						cnt := 10;
						outputs <= hub_port(cnt-1);
					when write_hub_port =>
						if cnt = 1 then
							outputs <= '1';
							d_cnt <= one;
							counter <= 0;
							if extd = '0' then
								state <= start_addr;
							else
								state <= start_hub_port_extd;
							end if;
						else
							cnt := cnt - 1;
							outputs <= hub_port(cnt-1);
							state <= write_hub_port;
						end if;
					-------------------
					when start_hub_port_extd => 
						state <= write_hub_port_extd;
						cnt := 10;
						outputs <= hub_port_extd(cnt-1);
					when write_hub_port_extd =>
						if cnt = 1 then
							outputs <= '1';
							state <= start_addr;
							d_cnt <= one;
							counter <= 0;
						else
							cnt := cnt - 1;
							outputs <= hub_port_extd(cnt-1);
							state <= write_hub_port_extd;
						end if;
					-------------------
					when start_addr =>
						if d_cnt = one and counter = 0 then
							state <= start_addr;
							outputs <= '0'; -- '1'
							d_cnt <= two;
							counter <= counter + 1;
						elsif d_cnt = two and counter = 1 then
							state <= start_addr;
							outputs <= '1'; -- '0'
							d_cnt <= one;
							counter <= counter + 1;
						else
							if counter = counter_max-1-2 -8 then
								state <= write_addr;
								outputs <= addr(cnt-1);
								d_cnt <= zero;
								counter <= 0;
							else
								state <= start_addr;
								outputs <= not outputs;
								d_cnt <= zero;
								counter <= counter + 1;
							end if;
						end if;
						cnt := 9;--10;
					when write_addr =>
						if cnt = 1 then
							if counter = counter_max-1 then
								counter <= 0;
								state <= write_data;
								cnt := 9;--10;
								outputs <= data(cnt-1);
							else
								counter <= counter + 1;
								state <= write_addr;
								-- outputs <= addr(0);
								outputs <= not outputs;
							end if;
						else
							if counter = counter_max-1 then
								cnt := cnt - 1;
								counter <= 0;
								outputs <= addr(cnt-1);
							else
								counter <= counter + 1;
								cnt := cnt;
								outputs <= not outputs;
							end if;
							state <= write_addr;
						end if;
					when write_data =>
						if cnt = 1 then
							if counter = counter_max-1 then
								counter <= 0;
								state <= stopp;--ack;
								outputs <= '0'; -- '1'
							else
								counter <= counter + 1;
								state <= write_data;
								outputs <= not outputs;
							end if;
						else
							if counter = counter_max-1 then
								cnt := cnt - 1;
								counter <= 0;
								outputs <= data(cnt-1);
							else
								counter <= counter + 1;
								cnt := cnt;
								outputs <= not outputs;
							end if;
							state <= write_data;
						end if;
					when ack =>
						if counter = counter_max-1 then
							cnt := cnt - 1;
							counter <= 0;
							state <= stopp;
							outputs <= '1'; -- '0'
						else
							counter <= counter + 1;
							cnt := cnt;
							state <= ack;
							outputs <= not outputs;
						end if;
					when stopp =>
						if counter > counter_max-1 then
							cnt := 10;
							counter <= 0;
							state <= idle;
							outputs <= '0'; -- '1'
						elsif counter >= 16 then -- prev 2
							outputs <= '1';
							cnt := cnt - 1;
							counter <= counter + 1;
							state <= stopp;
							outputs <= '0'; -- '1'
						else
							counter <= counter + 1;
							cnt := cnt;
							outputs <= '1'; -- '0'
						end if;
					when others =>
						state <= idle;
				end case;
				state_prev <= state;
			end if;
		end if;
	end process;	


	startstop_p : process( clk_90deg )
	begin
		if rising_edge( clk_90deg ) then
			if state = start_hub_port or state = start_addr or state = start_hub_port_extd then
				start_flg <= '0';
			else
				start_flg <= '1';
			end if;
		end if;
	end process;


	with state select ser_data <= 
		clk_90deg when idle,
		start_flg when start_hub_port,
		start_flg when start_hub_port_extd,
		not outputs when start_addr,
		not outputs when write_hub_port,
		not outputs when write_hub_port_extd,
		not outputs when write_addr,
		not outputs when write_data,
		not outputs when ack,
		not outputs when stopp,
		clk_90deg when others;


	slow_clk_p : process( clk )
		variable edge_cnt : integer;
	begin
		if rising_edge( clk ) then
			if state = start_addr and (state_prev = write_hub_port or state_prev = write_hub_port_extd) then
				slow_clk <= '1';
				slow_cnt <= 0;
				double_clk <= '1';
				edge_cnt := 1;
			elsif slow_cnt = (slow_cnt_max/2)-1 then
				slow_clk <= not slow_clk;
				slow_cnt <= 0;
				double_clk <= not double_clk;
			else
				slow_clk <= slow_clk;
				slow_cnt <= slow_cnt + 1;
				double_clk <= not double_clk;
			end if;
		end if;
	end process;

	latch_p : process( double_clk )
	begin
		if double_clk = '1' then
			latch <= outputs;
		end if;
	end process;

end Behavioral;

