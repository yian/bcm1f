library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
library unisim;
use unisim.vcomponents.all;
use work.system_package.all;

ENTITY i2c_master_dual_tb IS
END i2c_master_dual_tb;

ARCHITECTURE behavior OF i2c_master_dual_tb IS 

--	component i2c_master_dual
--	port(
--		clk				: in  	std_logic;
--		reset				: in  	std_logic;
--		-- master regs -
--		enable			: in  	std_logic;
--		bus_select		: in  	std_logic_vector( 2 downto 0):="000";
--		prescaler		: in  	std_logic_vector( 9 downto 0);
--		command			: in  	std_logic_vector(31 downto 0);
--		reply				: out 	std_logic_vector(31 downto 0);
--		-- IO pins -----
--		scl_io			:inout	std_logic_vector(1 downto 0);					
--		sda_io			:inout	std_logic_vector(1 downto 0)
--	); 			
--	end component;

	signal clk : std_logic;
	signal reset : std_logic;
	signal enable : std_logic;
	signal bus_select : std_logic_vector( 2 downto 0);
	signal prescaler : std_logic_vector( 9 downto 0);
	signal command : std_logic_vector(31 downto 0);
	signal reply : std_logic_vector(31 downto 0);
	signal scl_io : std_logic_vector(1 downto 0);
	signal sda_io : std_logic_vector(1 downto 0);
	
	constant clk_per : time := 25 ns;
	
BEGIN

	DUT : entity work.i2c_master_dual
	port map(
		clk => clk,
		reset => reset,
		enable => enable,
		bus_select => bus_select,
		prescaler => prescaler,
		command => command,
		reply => reply,
		scl_io => scl_io,
		sda_io => sda_io
	);
	
	rst_p : process
	begin
		reset <= '1';
		wait for 100 ns;
		reset <= '0';
		wait;
	end process;
	
	clk_p : process
	begin
		clk <= '1';
		wait for clk_per/2;
		clk <= '0';
		wait for clk_per/2;
	end process;
	
	-- I2C setting: 0x00000be8
	-- enable = 1
	-- bus select = 0
	-- prescaler = 1000 = 0x3e8 = 1111101000
	
	enable <= '1';
	bus_select <= "000";
	prescaler <= "1000000001"; --"1111101000";
	
	
	------------------------------
	--=== command  ==--
	------------------------------
--	executestrobe	=> command(31),
--	extmode			=> command(25), --16bit data
--	ralmode			=> command(24),
--	writetoslave	=> command(23),
--	slaveaddress	=> '0'&  command(22 downto 16),
--	slaveregister	=> command(15 downto 8 ),
--	datatoslave		=> command( 7 downto 0 ),
	
	write_p : process
	begin
		wait for 10000 ns;
		command(31) <= '1'; -- strobe
		command(25) <= '1'; -- extmode
		command(24) <= '1'; -- ralmode
		command(23) <= '1'; -- write
		command(22 downto 16) <= "1100110"; -- slaveaddress
		command(15 downto 8 ) <= "10101000"; -- slaveregister
		command( 7 downto 0 ) <= "11110000"; -- data		
		command( 30 downto 26 ) <= (others=>'0');
		wait for 50 ns;
		command(31) <= '0'; -- strobe
		wait;
		end process;

END;
