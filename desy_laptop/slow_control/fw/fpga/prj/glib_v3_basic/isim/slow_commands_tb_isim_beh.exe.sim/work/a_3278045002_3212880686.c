/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x8ef4fb42 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/slow_control/fw/fpga/src/user/usr/slow_control/slow_commands.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1258338084_503743352(char *, char *, unsigned int , unsigned int );
unsigned char ieee_p_2592010699_sub_1690584930_503743352(char *, unsigned char );
unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );


static void work_a_3278045002_3212880686_p_0(char *t0)
{
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    unsigned char t5;
    unsigned char t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;

LAB0:    xsi_set_current_line(52, ng0);
    t1 = (t0 + 660U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 4464);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(53, ng0);
    t3 = (t0 + 1328U);
    t4 = *((char **)t3);
    t5 = *((unsigned char *)t4);
    t6 = (t5 == (unsigned char)3);
    if (t6 != 0)
        goto LAB5;

LAB7:    xsi_set_current_line(56, ng0);
    t1 = (t0 + 4548);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);

LAB6:    xsi_set_current_line(58, ng0);
    t1 = (t0 + 2616U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t5 = (t2 == (unsigned char)3);
    if (t5 != 0)
        goto LAB8;

LAB10:    xsi_set_current_line(61, ng0);
    t1 = (t0 + 1328U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t1 = (t0 + 4584);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t2;
    xsi_driver_first_trans_fast(t1);

LAB9:    goto LAB3;

LAB5:    xsi_set_current_line(54, ng0);
    t3 = (t0 + 4548);
    t7 = (t3 + 32U);
    t8 = *((char **)t7);
    t9 = (t8 + 40U);
    t10 = *((char **)t9);
    *((unsigned char *)t10) = (unsigned char)3;
    xsi_driver_first_trans_fast(t3);
    goto LAB6;

LAB8:    xsi_set_current_line(59, ng0);
    t1 = (t0 + 4584);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    goto LAB9;

}

static void work_a_3278045002_3212880686_p_1(char *t0)
{
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    unsigned char t5;
    unsigned char t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    int t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    int t19;
    int t20;
    static char *nl0[] = {&&LAB9, &&LAB10, &&LAB11, &&LAB14, &&LAB15, &&LAB16, &&LAB12, &&LAB13, &&LAB17, &&LAB18};

LAB0:    xsi_set_current_line(69, ng0);
    t1 = (t0 + 660U);
    t2 = ieee_p_2592010699_sub_1258338084_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 4472);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(70, ng0);
    t3 = (t0 + 776U);
    t4 = *((char **)t3);
    t5 = *((unsigned char *)t4);
    t6 = (t5 == (unsigned char)3);
    if (t6 != 0)
        goto LAB5;

LAB7:    xsi_set_current_line(73, ng0);
    t1 = (t0 + 1604U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t1 = (char *)((nl0) + t2);
    goto **((char **)t1);

LAB5:    xsi_set_current_line(71, ng0);
    t3 = (t0 + 4620);
    t7 = (t3 + 32U);
    t8 = *((char **)t7);
    t9 = (t8 + 40U);
    t10 = *((char **)t9);
    *((unsigned char *)t10) = (unsigned char)0;
    xsi_driver_first_trans_fast(t3);

LAB6:    goto LAB3;

LAB8:    xsi_set_current_line(226, ng0);
    t1 = (t0 + 1604U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t1 = (t0 + 4800);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t2;
    xsi_driver_first_trans_fast(t1);
    goto LAB6;

LAB9:    xsi_set_current_line(75, ng0);
    t4 = (t0 + 1328U);
    t7 = *((char **)t4);
    t5 = *((unsigned char *)t7);
    t6 = (t5 == (unsigned char)3);
    if (t6 != 0)
        goto LAB20;

LAB22:    xsi_set_current_line(80, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(81, ng0);
    t1 = (t0 + 4656);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);

LAB21:    goto LAB8;

LAB10:    xsi_set_current_line(84, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(85, ng0);
    t1 = (t0 + 2924U);
    t3 = *((char **)t1);
    t1 = (t3 + 0);
    *((int *)t1) = 10;
    xsi_set_current_line(86, ng0);
    t1 = (t0 + 868U);
    t3 = *((char **)t1);
    t1 = (t0 + 2924U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t13 = (t12 - 1);
    t14 = (t13 - 9);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t3 + t17);
    t2 = *((unsigned char *)t1);
    t7 = (t0 + 4692);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t2;
    xsi_driver_first_trans_fast(t7);
    goto LAB8;

LAB11:    xsi_set_current_line(88, ng0);
    t1 = (t0 + 2924U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t2 = (t12 == 1);
    if (t2 != 0)
        goto LAB23;

LAB25:    xsi_set_current_line(98, ng0);
    t1 = (t0 + 2924U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 - 1);
    t1 = (t0 + 2924U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = t13;
    xsi_set_current_line(99, ng0);
    t1 = (t0 + 868U);
    t3 = *((char **)t1);
    t1 = (t0 + 2924U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t13 = (t12 - 1);
    t14 = (t13 - 9);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t3 + t17);
    t2 = *((unsigned char *)t1);
    t7 = (t0 + 4692);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t2;
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(100, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);

LAB24:    goto LAB8;

LAB12:    xsi_set_current_line(104, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)7;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(105, ng0);
    t1 = (t0 + 2924U);
    t3 = *((char **)t1);
    t1 = (t3 + 0);
    *((int *)t1) = 10;
    xsi_set_current_line(106, ng0);
    t1 = (t0 + 960U);
    t3 = *((char **)t1);
    t1 = (t0 + 2924U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t13 = (t12 - 1);
    t14 = (t13 - 9);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t3 + t17);
    t2 = *((unsigned char *)t1);
    t7 = (t0 + 4692);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t2;
    xsi_driver_first_trans_fast(t7);
    goto LAB8;

LAB13:    xsi_set_current_line(108, ng0);
    t1 = (t0 + 2924U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t2 = (t12 == 1);
    if (t2 != 0)
        goto LAB29;

LAB31:    xsi_set_current_line(114, ng0);
    t1 = (t0 + 2924U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 - 1);
    t1 = (t0 + 2924U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = t13;
    xsi_set_current_line(115, ng0);
    t1 = (t0 + 960U);
    t3 = *((char **)t1);
    t1 = (t0 + 2924U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t13 = (t12 - 1);
    t14 = (t13 - 9);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t3 + t17);
    t2 = *((unsigned char *)t1);
    t7 = (t0 + 4692);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t2;
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(116, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)7;
    xsi_driver_first_trans_fast(t1);

LAB30:    goto LAB8;

LAB14:    xsi_set_current_line(120, ng0);
    t1 = (t0 + 1972U);
    t3 = *((char **)t1);
    t5 = *((unsigned char *)t3);
    t6 = (t5 == (unsigned char)0);
    if (t6 == 1)
        goto LAB35;

LAB36:    t2 = (unsigned char)0;

LAB37:    if (t2 != 0)
        goto LAB32;

LAB34:    t1 = (t0 + 1972U);
    t3 = *((char **)t1);
    t5 = *((unsigned char *)t3);
    t6 = (t5 == (unsigned char)1);
    if (t6 == 1)
        goto LAB40;

LAB41:    t2 = (unsigned char)0;

LAB42:    if (t2 != 0)
        goto LAB38;

LAB39:    xsi_set_current_line(131, ng0);
    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t1 = (t0 + 2856U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t14 = (t13 - 1);
    t19 = (t14 - 2);
    t2 = (t12 == t19);
    if (t2 != 0)
        goto LAB43;

LAB45:    xsi_set_current_line(137, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(138, ng0);
    t1 = (t0 + 1788U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t5 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t2);
    t1 = (t0 + 4692);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t5;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(139, ng0);
    t1 = (t0 + 4728);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(140, ng0);
    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 + 1);
    t1 = (t0 + 4764);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((int *)t9) = t13;
    xsi_driver_first_trans_fast(t1);

LAB44:
LAB33:    xsi_set_current_line(143, ng0);
    t1 = (t0 + 2924U);
    t3 = *((char **)t1);
    t1 = (t3 + 0);
    *((int *)t1) = 10;
    goto LAB8;

LAB15:    xsi_set_current_line(145, ng0);
    t1 = (t0 + 2924U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t2 = (t12 == 1);
    if (t2 != 0)
        goto LAB46;

LAB48:    xsi_set_current_line(158, ng0);
    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t1 = (t0 + 2856U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t14 = (t13 - 1);
    t2 = (t12 == t14);
    if (t2 != 0)
        goto LAB52;

LAB54:    xsi_set_current_line(163, ng0);
    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 + 1);
    t1 = (t0 + 4764);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((int *)t9) = t13;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(164, ng0);
    t1 = (t0 + 2924U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t1 = (t0 + 2924U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = t12;
    xsi_set_current_line(165, ng0);
    t1 = (t0 + 1788U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t5 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t2);
    t1 = (t0 + 4692);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t5;
    xsi_driver_first_trans_fast(t1);

LAB53:    xsi_set_current_line(167, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)4;
    xsi_driver_first_trans_fast(t1);

LAB47:    goto LAB8;

LAB16:    xsi_set_current_line(170, ng0);
    t1 = (t0 + 2924U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t2 = (t12 == 1);
    if (t2 != 0)
        goto LAB55;

LAB57:    xsi_set_current_line(182, ng0);
    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t1 = (t0 + 2856U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t14 = (t13 - 1);
    t2 = (t12 == t14);
    if (t2 != 0)
        goto LAB61;

LAB63:    xsi_set_current_line(187, ng0);
    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 + 1);
    t1 = (t0 + 4764);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((int *)t9) = t13;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(188, ng0);
    t1 = (t0 + 2924U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t1 = (t0 + 2924U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = t12;
    xsi_set_current_line(189, ng0);
    t1 = (t0 + 1788U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t5 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t2);
    t1 = (t0 + 4692);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t5;
    xsi_driver_first_trans_fast(t1);

LAB62:    xsi_set_current_line(191, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)5;
    xsi_driver_first_trans_fast(t1);

LAB56:    goto LAB8;

LAB17:    xsi_set_current_line(194, ng0);
    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t1 = (t0 + 2856U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t14 = (t13 - 1);
    t2 = (t12 == t14);
    if (t2 != 0)
        goto LAB64;

LAB66:    xsi_set_current_line(200, ng0);
    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 + 1);
    t1 = (t0 + 4764);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((int *)t9) = t13;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(201, ng0);
    t1 = (t0 + 2924U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t1 = (t0 + 2924U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = t12;
    xsi_set_current_line(202, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)8;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(204, ng0);
    t1 = (t0 + 1788U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t5 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t2);
    t1 = (t0 + 4692);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t5;
    xsi_driver_first_trans_fast(t1);

LAB65:    goto LAB8;

LAB18:    xsi_set_current_line(207, ng0);
    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t1 = (t0 + 2856U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t14 = (t13 - 1);
    t2 = (t12 > t14);
    if (t2 != 0)
        goto LAB67;

LAB69:    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t2 = (t12 >= 2);
    if (t2 != 0)
        goto LAB70;

LAB71:    xsi_set_current_line(219, ng0);
    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 + 1);
    t1 = (t0 + 4764);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((int *)t9) = t13;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(220, ng0);
    t1 = (t0 + 2924U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t1 = (t0 + 2924U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = t12;
    xsi_set_current_line(221, ng0);
    t1 = (t0 + 4692);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);

LAB68:    goto LAB8;

LAB19:    xsi_set_current_line(224, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    goto LAB8;

LAB20:    xsi_set_current_line(77, ng0);
    t4 = (t0 + 4620);
    t8 = (t4 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = (unsigned char)1;
    xsi_driver_first_trans_fast(t4);
    xsi_set_current_line(78, ng0);
    t1 = (t0 + 4656);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB21;

LAB23:    xsi_set_current_line(89, ng0);
    t1 = (t0 + 4692);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(90, ng0);
    t1 = (t0 + 4728);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(91, ng0);
    t1 = (t0 + 4764);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((int *)t8) = 0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(92, ng0);
    t1 = (t0 + 1052U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t5 = (t2 == (unsigned char)2);
    if (t5 != 0)
        goto LAB26;

LAB28:    xsi_set_current_line(95, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)6;
    xsi_driver_first_trans_fast(t1);

LAB27:    goto LAB24;

LAB26:    xsi_set_current_line(93, ng0);
    t1 = (t0 + 4620);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    goto LAB27;

LAB29:    xsi_set_current_line(109, ng0);
    t1 = (t0 + 4692);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(110, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(111, ng0);
    t1 = (t0 + 4728);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(112, ng0);
    t1 = (t0 + 4764);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((int *)t8) = 0;
    xsi_driver_first_trans_fast(t1);
    goto LAB30;

LAB32:    xsi_set_current_line(121, ng0);
    t1 = (t0 + 4620);
    t7 = (t1 + 32U);
    t8 = *((char **)t7);
    t9 = (t8 + 40U);
    t10 = *((char **)t9);
    *((unsigned char *)t10) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(122, ng0);
    t1 = (t0 + 4692);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(123, ng0);
    t1 = (t0 + 4728);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)1;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(124, ng0);
    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 + 1);
    t1 = (t0 + 4764);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((int *)t9) = t13;
    xsi_driver_first_trans_fast(t1);
    goto LAB33;

LAB35:    t1 = (t0 + 2340U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t18 = (t12 == 0);
    t2 = t18;
    goto LAB37;

LAB38:    xsi_set_current_line(126, ng0);
    t1 = (t0 + 4620);
    t7 = (t1 + 32U);
    t8 = *((char **)t7);
    t9 = (t8 + 40U);
    t10 = *((char **)t9);
    *((unsigned char *)t10) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(127, ng0);
    t1 = (t0 + 4692);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(128, ng0);
    t1 = (t0 + 4728);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(129, ng0);
    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 + 1);
    t1 = (t0 + 4764);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((int *)t9) = t13;
    xsi_driver_first_trans_fast(t1);
    goto LAB33;

LAB40:    t1 = (t0 + 2340U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t18 = (t12 == 1);
    t2 = t18;
    goto LAB42;

LAB43:    xsi_set_current_line(132, ng0);
    t1 = (t0 + 4620);
    t7 = (t1 + 32U);
    t8 = *((char **)t7);
    t9 = (t8 + 40U);
    t10 = *((char **)t9);
    *((unsigned char *)t10) = (unsigned char)4;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(133, ng0);
    t1 = (t0 + 1144U);
    t3 = *((char **)t1);
    t1 = (t0 + 2924U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t13 = (t12 - 1);
    t14 = (t13 - 9);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t3 + t17);
    t2 = *((unsigned char *)t1);
    t7 = (t0 + 4692);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t2;
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(134, ng0);
    t1 = (t0 + 4728);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(135, ng0);
    t1 = (t0 + 4764);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((int *)t8) = 0;
    xsi_driver_first_trans_fast(t1);
    goto LAB44;

LAB46:    xsi_set_current_line(146, ng0);
    t1 = (t0 + 2340U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t1 = (t0 + 2856U);
    t7 = *((char **)t1);
    t14 = *((int *)t7);
    t19 = (t14 - 1);
    t5 = (t13 == t19);
    if (t5 != 0)
        goto LAB49;

LAB51:    xsi_set_current_line(152, ng0);
    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 + 1);
    t1 = (t0 + 4764);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((int *)t9) = t13;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(153, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)4;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(155, ng0);
    t1 = (t0 + 1788U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t5 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t2);
    t1 = (t0 + 4692);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t5;
    xsi_driver_first_trans_fast(t1);

LAB50:    goto LAB47;

LAB49:    xsi_set_current_line(147, ng0);
    t1 = (t0 + 4764);
    t8 = (t1 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((int *)t11) = 0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(148, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)5;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(149, ng0);
    t1 = (t0 + 2924U);
    t3 = *((char **)t1);
    t1 = (t3 + 0);
    *((int *)t1) = 10;
    xsi_set_current_line(150, ng0);
    t1 = (t0 + 1236U);
    t3 = *((char **)t1);
    t1 = (t0 + 2924U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t13 = (t12 - 1);
    t14 = (t13 - 9);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t3 + t17);
    t2 = *((unsigned char *)t1);
    t7 = (t0 + 4692);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t2;
    xsi_driver_first_trans_fast(t7);
    goto LAB50;

LAB52:    xsi_set_current_line(159, ng0);
    t1 = (t0 + 2924U);
    t7 = *((char **)t1);
    t19 = *((int *)t7);
    t20 = (t19 - 1);
    t1 = (t0 + 2924U);
    t8 = *((char **)t1);
    t1 = (t8 + 0);
    *((int *)t1) = t20;
    xsi_set_current_line(160, ng0);
    t1 = (t0 + 4764);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((int *)t8) = 0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(161, ng0);
    t1 = (t0 + 1144U);
    t3 = *((char **)t1);
    t1 = (t0 + 2924U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t13 = (t12 - 1);
    t14 = (t13 - 9);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t3 + t17);
    t2 = *((unsigned char *)t1);
    t7 = (t0 + 4692);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t2;
    xsi_driver_first_trans_fast(t7);
    goto LAB53;

LAB55:    xsi_set_current_line(171, ng0);
    t1 = (t0 + 2340U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t1 = (t0 + 2856U);
    t7 = *((char **)t1);
    t14 = *((int *)t7);
    t19 = (t14 - 1);
    t5 = (t13 == t19);
    if (t5 != 0)
        goto LAB58;

LAB60:    xsi_set_current_line(176, ng0);
    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 + 1);
    t1 = (t0 + 4764);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((int *)t9) = t13;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(177, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)5;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(179, ng0);
    t1 = (t0 + 1788U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t5 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t2);
    t1 = (t0 + 4692);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t5;
    xsi_driver_first_trans_fast(t1);

LAB59:    goto LAB56;

LAB58:    xsi_set_current_line(172, ng0);
    t1 = (t0 + 4764);
    t8 = (t1 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((int *)t11) = 0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(173, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)8;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(174, ng0);
    t1 = (t0 + 4692);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    goto LAB59;

LAB61:    xsi_set_current_line(183, ng0);
    t1 = (t0 + 2924U);
    t7 = *((char **)t1);
    t19 = *((int *)t7);
    t20 = (t19 - 1);
    t1 = (t0 + 2924U);
    t8 = *((char **)t1);
    t1 = (t8 + 0);
    *((int *)t1) = t20;
    xsi_set_current_line(184, ng0);
    t1 = (t0 + 4764);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((int *)t8) = 0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(185, ng0);
    t1 = (t0 + 1236U);
    t3 = *((char **)t1);
    t1 = (t0 + 2924U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t13 = (t12 - 1);
    t14 = (t13 - 9);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t3 + t17);
    t2 = *((unsigned char *)t1);
    t7 = (t0 + 4692);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t2;
    xsi_driver_first_trans_fast(t7);
    goto LAB62;

LAB64:    xsi_set_current_line(195, ng0);
    t1 = (t0 + 2924U);
    t7 = *((char **)t1);
    t19 = *((int *)t7);
    t20 = (t19 - 1);
    t1 = (t0 + 2924U);
    t8 = *((char **)t1);
    t1 = (t8 + 0);
    *((int *)t1) = t20;
    xsi_set_current_line(196, ng0);
    t1 = (t0 + 4764);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((int *)t8) = 0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(197, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)9;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(198, ng0);
    t1 = (t0 + 4692);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    goto LAB65;

LAB67:    xsi_set_current_line(208, ng0);
    t1 = (t0 + 2924U);
    t7 = *((char **)t1);
    t19 = *((int *)t7);
    t20 = (t19 - 1);
    t1 = (t0 + 2924U);
    t8 = *((char **)t1);
    t1 = (t8 + 0);
    *((int *)t1) = t20;
    xsi_set_current_line(209, ng0);
    t1 = (t0 + 4764);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((int *)t8) = 0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(210, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(211, ng0);
    t1 = (t0 + 4692);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    goto LAB68;

LAB70:    xsi_set_current_line(213, ng0);
    t1 = (t0 + 4692);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(214, ng0);
    t1 = (t0 + 2924U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 - 1);
    t1 = (t0 + 2924U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = t13;
    xsi_set_current_line(215, ng0);
    t1 = (t0 + 2340U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 + 1);
    t1 = (t0 + 4764);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((int *)t9) = t13;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(216, ng0);
    t1 = (t0 + 4620);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)9;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(217, ng0);
    t1 = (t0 + 4692);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    goto LAB68;

}

static void work_a_3278045002_3212880686_p_2(char *t0)
{
    char *t1;
    unsigned char t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    unsigned char t7;
    unsigned char t8;
    char *t9;
    unsigned char t10;
    unsigned char t11;
    char *t12;
    unsigned char t13;
    unsigned char t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    xsi_set_current_line(234, ng0);
    t1 = (t0 + 660U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 4480);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(235, ng0);
    t5 = (t0 + 1604U);
    t6 = *((char **)t5);
    t7 = *((unsigned char *)t6);
    t8 = (t7 == (unsigned char)1);
    if (t8 == 1)
        goto LAB11;

LAB12:    t5 = (t0 + 1604U);
    t9 = *((char **)t5);
    t10 = *((unsigned char *)t9);
    t11 = (t10 == (unsigned char)3);
    t4 = t11;

LAB13:    if (t4 == 1)
        goto LAB8;

LAB9:    t5 = (t0 + 1604U);
    t12 = *((char **)t5);
    t13 = *((unsigned char *)t12);
    t14 = (t13 == (unsigned char)6);
    t3 = t14;

LAB10:    if (t3 != 0)
        goto LAB5;

LAB7:    xsi_set_current_line(238, ng0);
    t1 = (t0 + 4836);
    t5 = (t1 + 32U);
    t6 = *((char **)t5);
    t9 = (t6 + 40U);
    t12 = *((char **)t9);
    *((unsigned char *)t12) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);

LAB6:    goto LAB3;

LAB5:    xsi_set_current_line(236, ng0);
    t5 = (t0 + 4836);
    t15 = (t5 + 32U);
    t16 = *((char **)t15);
    t17 = (t16 + 40U);
    t18 = *((char **)t17);
    *((unsigned char *)t18) = (unsigned char)2;
    xsi_driver_first_trans_fast(t5);
    goto LAB6;

LAB8:    t3 = (unsigned char)1;
    goto LAB10;

LAB11:    t4 = (unsigned char)1;
    goto LAB13;

}

static void work_a_3278045002_3212880686_p_3(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    unsigned char t4;
    char *t5;
    char *t6;
    unsigned char t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    static char *nl0[] = {&&LAB5, &&LAB6, &&LAB9, &&LAB8, &&LAB11, &&LAB12, &&LAB7, &&LAB10, &&LAB13, &&LAB14};

LAB0:    t1 = (t0 + 3980U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(244, ng0);
    t2 = (t0 + 1604U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t2 = (char *)((nl0) + t4);
    goto **((char **)t2);

LAB4:    xsi_set_current_line(244, ng0);

LAB18:    t2 = (t0 + 4488);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB19;

LAB1:    return;
LAB5:    xsi_set_current_line(245, ng0);
    t5 = (t0 + 684U);
    t6 = *((char **)t5);
    t7 = *((unsigned char *)t6);
    t5 = (t0 + 4872);
    t8 = (t5 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t7;
    xsi_driver_first_trans_fast_port(t5);
    goto LAB4;

LAB6:    xsi_set_current_line(245, ng0);
    t2 = (t0 + 1880U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t2 = (t0 + 4872);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t4;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB7:    xsi_set_current_line(245, ng0);
    t2 = (t0 + 1880U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t2 = (t0 + 4872);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t4;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB8:    xsi_set_current_line(245, ng0);
    t2 = (t0 + 1788U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t7 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t4);
    t2 = (t0 + 4872);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t7;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB9:    xsi_set_current_line(245, ng0);
    t2 = (t0 + 1788U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t7 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t4);
    t2 = (t0 + 4872);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t7;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB10:    xsi_set_current_line(245, ng0);
    t2 = (t0 + 1788U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t7 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t4);
    t2 = (t0 + 4872);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t7;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB11:    xsi_set_current_line(245, ng0);
    t2 = (t0 + 1788U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t7 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t4);
    t2 = (t0 + 4872);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t7;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB12:    xsi_set_current_line(245, ng0);
    t2 = (t0 + 1788U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t7 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t4);
    t2 = (t0 + 4872);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t7;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB13:    xsi_set_current_line(245, ng0);
    t2 = (t0 + 1788U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t7 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t4);
    t2 = (t0 + 4872);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t7;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB14:    xsi_set_current_line(245, ng0);
    t2 = (t0 + 1788U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t7 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t4);
    t2 = (t0 + 4872);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t7;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB15:    xsi_set_current_line(245, ng0);
    t2 = (t0 + 684U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t2 = (t0 + 4872);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t4;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB16:    t3 = (t0 + 4488);
    *((int *)t3) = 0;
    goto LAB2;

LAB17:    goto LAB16;

LAB19:    goto LAB17;

}

static void work_a_3278045002_3212880686_p_4(char *t0)
{
    char *t1;
    unsigned char t2;
    unsigned char t3;
    char *t4;
    char *t5;
    unsigned char t6;
    unsigned char t7;
    unsigned char t8;
    char *t9;
    unsigned char t10;
    unsigned char t11;
    char *t12;
    unsigned char t13;
    unsigned char t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    int t19;
    int t20;
    int t21;
    int t22;

LAB0:    xsi_set_current_line(261, ng0);
    t1 = (t0 + 568U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 4496);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(262, ng0);
    t4 = (t0 + 1604U);
    t5 = *((char **)t4);
    t6 = *((unsigned char *)t5);
    t7 = (t6 == (unsigned char)3);
    if (t7 == 1)
        goto LAB8;

LAB9:    t3 = (unsigned char)0;

LAB10:    if (t3 != 0)
        goto LAB5;

LAB7:    t1 = (t0 + 2156U);
    t4 = *((char **)t1);
    t19 = *((int *)t4);
    t1 = (t0 + 2788U);
    t5 = *((char **)t1);
    t20 = *((int *)t5);
    t21 = (t20 / 2);
    t22 = (t21 - 1);
    t2 = (t19 == t22);
    if (t2 != 0)
        goto LAB14;

LAB15:    xsi_set_current_line(272, ng0);
    t1 = (t0 + 2248U);
    t4 = *((char **)t1);
    t2 = *((unsigned char *)t4);
    t1 = (t0 + 4908);
    t5 = (t1 + 32U);
    t9 = *((char **)t5);
    t12 = (t9 + 40U);
    t15 = *((char **)t12);
    *((unsigned char *)t15) = t2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(273, ng0);
    t1 = (t0 + 2156U);
    t4 = *((char **)t1);
    t19 = *((int *)t4);
    t20 = (t19 + 1);
    t1 = (t0 + 4944);
    t5 = (t1 + 32U);
    t9 = *((char **)t5);
    t12 = (t9 + 40U);
    t15 = *((char **)t12);
    *((int *)t15) = t20;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(274, ng0);
    t1 = (t0 + 2432U);
    t4 = *((char **)t1);
    t2 = *((unsigned char *)t4);
    t3 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t2);
    t1 = (t0 + 4980);
    t5 = (t1 + 32U);
    t9 = *((char **)t5);
    t12 = (t9 + 40U);
    t15 = *((char **)t12);
    *((unsigned char *)t15) = t3;
    xsi_driver_first_trans_fast(t1);

LAB6:    goto LAB3;

LAB5:    xsi_set_current_line(263, ng0);
    t4 = (t0 + 4908);
    t15 = (t4 + 32U);
    t16 = *((char **)t15);
    t17 = (t16 + 40U);
    t18 = *((char **)t17);
    *((unsigned char *)t18) = (unsigned char)3;
    xsi_driver_first_trans_fast(t4);
    xsi_set_current_line(264, ng0);
    t1 = (t0 + 4944);
    t4 = (t1 + 32U);
    t5 = *((char **)t4);
    t9 = (t5 + 40U);
    t12 = *((char **)t9);
    *((int *)t12) = 0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(265, ng0);
    t1 = (t0 + 4980);
    t4 = (t1 + 32U);
    t5 = *((char **)t4);
    t9 = (t5 + 40U);
    t12 = *((char **)t9);
    *((unsigned char *)t12) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(266, ng0);
    t1 = (t0 + 2992U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = 1;
    goto LAB6;

LAB8:    t4 = (t0 + 1696U);
    t9 = *((char **)t4);
    t10 = *((unsigned char *)t9);
    t11 = (t10 == (unsigned char)2);
    if (t11 == 1)
        goto LAB11;

LAB12:    t4 = (t0 + 1696U);
    t12 = *((char **)t4);
    t13 = *((unsigned char *)t12);
    t14 = (t13 == (unsigned char)7);
    t8 = t14;

LAB13:    t3 = t8;
    goto LAB10;

LAB11:    t8 = (unsigned char)1;
    goto LAB13;

LAB14:    xsi_set_current_line(268, ng0);
    t1 = (t0 + 2248U);
    t9 = *((char **)t1);
    t3 = *((unsigned char *)t9);
    t6 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t3);
    t1 = (t0 + 4908);
    t12 = (t1 + 32U);
    t15 = *((char **)t12);
    t16 = (t15 + 40U);
    t17 = *((char **)t16);
    *((unsigned char *)t17) = t6;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(269, ng0);
    t1 = (t0 + 4944);
    t4 = (t1 + 32U);
    t5 = *((char **)t4);
    t9 = (t5 + 40U);
    t12 = *((char **)t9);
    *((int *)t12) = 0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(270, ng0);
    t1 = (t0 + 2432U);
    t4 = *((char **)t1);
    t2 = *((unsigned char *)t4);
    t3 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t2);
    t1 = (t0 + 4980);
    t5 = (t1 + 32U);
    t9 = *((char **)t5);
    t12 = (t9 + 40U);
    t15 = *((char **)t12);
    *((unsigned char *)t15) = t3;
    xsi_driver_first_trans_fast(t1);
    goto LAB6;

}

static void work_a_3278045002_3212880686_p_5(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    unsigned char t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;

LAB0:    xsi_set_current_line(281, ng0);
    t1 = (t0 + 2432U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 4504);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(282, ng0);
    t1 = (t0 + 1788U);
    t5 = *((char **)t1);
    t6 = *((unsigned char *)t5);
    t1 = (t0 + 5016);
    t7 = (t1 + 32U);
    t8 = *((char **)t7);
    t9 = (t8 + 40U);
    t10 = *((char **)t9);
    *((unsigned char *)t10) = t6;
    xsi_driver_first_trans_fast(t1);
    goto LAB3;

}


extern void work_a_3278045002_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3278045002_3212880686_p_0,(void *)work_a_3278045002_3212880686_p_1,(void *)work_a_3278045002_3212880686_p_2,(void *)work_a_3278045002_3212880686_p_3,(void *)work_a_3278045002_3212880686_p_4,(void *)work_a_3278045002_3212880686_p_5};
	xsi_register_didat("work_a_3278045002_3212880686", "isim/slow_commands_tb_isim_beh.exe.sim/work/a_3278045002_3212880686.didat");
	xsi_register_executes(pe);
}
