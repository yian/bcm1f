/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/aruede/Firmware/slow_hub_controller/ipbus_2_0_v1/firmware/slaves/hdl/ipbus_syncreg.vhd";
extern char *IEEE_P_1242562249;
extern char *IEEE_P_2592010699;

int ieee_p_1242562249_sub_1657552908_1035706684(char *, char *, char *);
unsigned char ieee_p_2592010699_sub_1605435078_503743352(char *, unsigned char , unsigned char );
unsigned char ieee_p_2592010699_sub_1690584930_503743352(char *, unsigned char );
unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
unsigned char ieee_p_2592010699_sub_2545490612_503743352(char *, unsigned char , unsigned char );


static void work_a_2748367590_1516540902_p_0(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;

LAB0:    xsi_set_current_line(57, ng0);
    t1 = (1 > 3);
    if (t1 != 0)
        goto LAB3;

LAB4:
LAB5:    t7 = (t0 + 15336);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t10 = (t9 + 56U);
    t11 = *((char **)t10);
    *((int *)t11) = 3;
    xsi_driver_first_trans_fast(t7);

LAB2:
LAB1:    return;
LAB3:    t2 = (t0 + 15336);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((int *)t6) = 1;
    xsi_driver_first_trans_fast(t2);
    goto LAB2;

LAB6:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_1(char *t0)
{
    char t9[16];
    unsigned char t1;
    char *t2;
    char *t3;
    int t4;
    unsigned int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t10;
    char *t11;
    int t12;
    unsigned int t13;
    int t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;

LAB0:    xsi_set_current_line(58, ng0);
    t1 = (1 > 0);
    if (t1 != 0)
        goto LAB3;

LAB4:
LAB5:    t19 = (t0 + 15400);
    t20 = (t19 + 56U);
    t21 = *((char **)t20);
    t22 = (t21 + 56U);
    t23 = *((char **)t22);
    *((int *)t23) = 0;
    xsi_driver_first_trans_fast(t19);

LAB2:    t24 = (t0 + 14984);
    *((int *)t24) = 1;

LAB1:    return;
LAB3:    t2 = (t0 + 3992U);
    t3 = *((char **)t2);
    t4 = (1 - 1);
    t5 = (31 - t4);
    t6 = (t5 * 1U);
    t7 = (0 + 0U);
    t8 = (t7 + t6);
    t2 = (t3 + t8);
    t10 = (t9 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 0;
    t11 = (t10 + 8U);
    *((int *)t11) = -1;
    t12 = (0 - 0);
    t13 = (t12 * -1);
    t13 = (t13 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t13;
    t14 = ieee_p_1242562249_sub_1657552908_1035706684(IEEE_P_1242562249, t2, t9);
    t11 = (t0 + 15400);
    t15 = (t11 + 56U);
    t16 = *((char **)t15);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    *((int *)t18) = t14;
    xsi_driver_first_trans_fast(t11);
    goto LAB2;

LAB6:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_2(char *t0)
{
    char t9[16];
    unsigned char t1;
    char *t2;
    char *t3;
    int t4;
    unsigned int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t10;
    char *t11;
    int t12;
    unsigned int t13;
    int t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;

LAB0:    xsi_set_current_line(59, ng0);
    t1 = (3 > 0);
    if (t1 != 0)
        goto LAB3;

LAB4:
LAB5:    t19 = (t0 + 15464);
    t20 = (t19 + 56U);
    t21 = *((char **)t20);
    t22 = (t21 + 56U);
    t23 = *((char **)t22);
    *((int *)t23) = 0;
    xsi_driver_first_trans_fast(t19);

LAB2:    t24 = (t0 + 15000);
    *((int *)t24) = 1;

LAB1:    return;
LAB3:    t2 = (t0 + 3992U);
    t3 = *((char **)t2);
    t4 = (3 - 1);
    t5 = (31 - t4);
    t6 = (t5 * 1U);
    t7 = (0 + 0U);
    t8 = (t7 + t6);
    t2 = (t3 + t8);
    t10 = (t9 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 2;
    t11 = (t10 + 4U);
    *((int *)t11) = 0;
    t11 = (t10 + 8U);
    *((int *)t11) = -1;
    t12 = (0 - 2);
    t13 = (t12 * -1);
    t13 = (t13 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t13;
    t14 = ieee_p_1242562249_sub_1657552908_1035706684(IEEE_P_1242562249, t2, t9);
    t11 = (t0 + 15464);
    t15 = (t11 + 56U);
    t16 = *((char **)t15);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    *((int *)t18) = t14;
    xsi_driver_first_trans_fast(t11);
    goto LAB2;

LAB6:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_3(char *t0)
{
    char *t1;
    char *t2;
    unsigned int t3;
    unsigned char t4;
    char *t5;
    char *t6;
    unsigned int t7;
    unsigned char t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned char t19;
    unsigned char t20;
    unsigned char t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(61, ng0);

LAB3:    t1 = (t0 + 3992U);
    t2 = *((char **)t1);
    t3 = (0 + 64U);
    t1 = (t2 + t3);
    t4 = *((unsigned char *)t1);
    t5 = (t0 + 3992U);
    t6 = *((char **)t5);
    t7 = (0 + 65U);
    t5 = (t6 + t7);
    t8 = *((unsigned char *)t5);
    t9 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t4, t8);
    t10 = (t0 + 3992U);
    t11 = *((char **)t10);
    t10 = (t0 + 5272U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 31);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(31, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + 0U);
    t18 = (t17 + t16);
    t10 = (t11 + t18);
    t19 = *((unsigned char *)t10);
    t20 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t19);
    t21 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t9, t20);
    t22 = (t0 + 15528);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    *((unsigned char *)t26) = t21;
    xsi_driver_first_trans_fast(t22);

LAB2:    t27 = (t0 + 15016);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_4(char *t0)
{
    char *t1;
    char *t2;
    unsigned int t3;
    unsigned char t4;
    char *t5;
    char *t6;
    unsigned int t7;
    unsigned char t8;
    unsigned char t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    int t14;
    int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned char t20;
    unsigned char t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;

LAB0:    xsi_set_current_line(62, ng0);

LAB3:    t1 = (t0 + 3992U);
    t2 = *((char **)t1);
    t3 = (0 + 64U);
    t1 = (t2 + t3);
    t4 = *((unsigned char *)t1);
    t5 = (t0 + 3992U);
    t6 = *((char **)t5);
    t7 = (0 + 65U);
    t5 = (t6 + t7);
    t8 = *((unsigned char *)t5);
    t9 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t8);
    t10 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t4, t9);
    t11 = (t0 + 3992U);
    t12 = *((char **)t11);
    t11 = (t0 + 5272U);
    t13 = *((char **)t11);
    t14 = *((int *)t13);
    t15 = (t14 - 31);
    t16 = (t15 * -1);
    xsi_vhdl_check_range_of_index(31, 0, -1, t14);
    t17 = (1U * t16);
    t18 = (0 + 0U);
    t19 = (t18 + t17);
    t11 = (t12 + t19);
    t20 = *((unsigned char *)t11);
    t21 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t20);
    t22 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t10, t21);
    t23 = (t0 + 15592);
    t24 = (t23 + 56U);
    t25 = *((char **)t24);
    t26 = (t25 + 56U);
    t27 = *((char **)t26);
    *((unsigned char *)t27) = t22;
    xsi_driver_first_trans_fast(t23);

LAB2:    t28 = (t0 + 15032);
    *((int *)t28) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_5(char *t0)
{
    char *t1;
    char *t2;
    unsigned int t3;
    unsigned char t4;
    char *t5;
    char *t6;
    unsigned int t7;
    unsigned char t8;
    unsigned char t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    int t14;
    int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned char t20;
    unsigned char t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(63, ng0);

LAB3:    t1 = (t0 + 3992U);
    t2 = *((char **)t1);
    t3 = (0 + 64U);
    t1 = (t2 + t3);
    t4 = *((unsigned char *)t1);
    t5 = (t0 + 3992U);
    t6 = *((char **)t5);
    t7 = (0 + 65U);
    t5 = (t6 + t7);
    t8 = *((unsigned char *)t5);
    t9 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t8);
    t10 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t4, t9);
    t11 = (t0 + 3992U);
    t12 = *((char **)t11);
    t11 = (t0 + 5272U);
    t13 = *((char **)t11);
    t14 = *((int *)t13);
    t15 = (t14 - 31);
    t16 = (t15 * -1);
    xsi_vhdl_check_range_of_index(31, 0, -1, t14);
    t17 = (1U * t16);
    t18 = (0 + 0U);
    t19 = (t18 + t17);
    t11 = (t12 + t19);
    t20 = *((unsigned char *)t11);
    t21 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t10, t20);
    t22 = (t0 + 15656);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    *((unsigned char *)t26) = t21;
    xsi_driver_first_trans_fast(t22);

LAB2:    t27 = (t0 + 15048);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_6(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    int t7;
    char *t8;
    int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;

LAB0:    xsi_set_current_line(67, ng0);
    t2 = (t0 + 5432U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = (t4 == (unsigned char)3);
    if (t5 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB3;

LAB4:
LAB8:    t15 = (t0 + 15720);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    *((unsigned char *)t19) = (unsigned char)2;
    xsi_driver_first_trans_delta(t15, 0U, 1, 0LL);

LAB2:    t20 = (t0 + 15064);
    *((int *)t20) = 1;

LAB1:    return;
LAB3:    t2 = (t0 + 15720);
    t11 = (t2 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)3;
    xsi_driver_first_trans_delta(t2, 0U, 1, 0LL);
    goto LAB2;

LAB5:    t2 = (t0 + 4952U);
    t6 = *((char **)t2);
    t7 = *((int *)t6);
    t2 = (t0 + 7888U);
    t8 = *((char **)t2);
    t9 = *((int *)t8);
    t10 = (t7 == t9);
    t1 = t10;
    goto LAB7;

LAB9:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_7(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    int t7;
    char *t8;
    int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;

LAB0:    xsi_set_current_line(67, ng0);
    t2 = (t0 + 5432U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = (t4 == (unsigned char)3);
    if (t5 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB3;

LAB4:
LAB8:    t15 = (t0 + 15784);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    *((unsigned char *)t19) = (unsigned char)2;
    xsi_driver_first_trans_delta(t15, 1U, 1, 0LL);

LAB2:    t20 = (t0 + 15080);
    *((int *)t20) = 1;

LAB1:    return;
LAB3:    t2 = (t0 + 15784);
    t11 = (t2 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)3;
    xsi_driver_first_trans_delta(t2, 1U, 1, 0LL);
    goto LAB2;

LAB5:    t2 = (t0 + 4952U);
    t6 = *((char **)t2);
    t7 = *((int *)t6);
    t2 = (t0 + 8008U);
    t8 = *((char **)t2);
    t9 = *((int *)t8);
    t10 = (t7 == t9);
    t1 = t10;
    goto LAB7;

LAB9:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_8(char *t0)
{
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    unsigned char t5;
    char *t6;
    unsigned char t7;
    char *t8;
    char *t9;
    int t10;
    int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned char t15;
    unsigned char t16;
    unsigned char t17;
    unsigned char t18;
    char *t19;
    char *t20;
    unsigned char t21;
    unsigned char t22;
    char *t23;
    char *t24;
    int t25;
    int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned char t30;
    unsigned char t31;
    unsigned char t32;
    char *t33;
    char *t34;
    char *t35;
    char *t36;
    char *t37;

LAB0:    xsi_set_current_line(87, ng0);
    t1 = (t0 + 3632U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 15096);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(88, ng0);
    t3 = (t0 + 6232U);
    t4 = *((char **)t3);
    t5 = *((unsigned char *)t4);
    t3 = (t0 + 5432U);
    t6 = *((char **)t3);
    t7 = *((unsigned char *)t6);
    t3 = (t0 + 6552U);
    t8 = *((char **)t3);
    t3 = (t0 + 4952U);
    t9 = *((char **)t3);
    t10 = *((int *)t9);
    t11 = (t10 - 1);
    t12 = (t11 * -1);
    xsi_vhdl_check_range_of_index(1, 0, -1, t10);
    t13 = (1U * t12);
    t14 = (0 + t13);
    t3 = (t8 + t14);
    t15 = *((unsigned char *)t3);
    t16 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t15);
    t17 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t7, t16);
    t18 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t5, t17);
    t19 = (t0 + 5432U);
    t20 = *((char **)t19);
    t21 = *((unsigned char *)t20);
    t22 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t18, t21);
    t19 = (t0 + 6712U);
    t23 = *((char **)t19);
    t19 = (t0 + 4952U);
    t24 = *((char **)t19);
    t25 = *((int *)t24);
    t26 = (t25 - 1);
    t27 = (t26 * -1);
    xsi_vhdl_check_range_of_index(1, 0, -1, t25);
    t28 = (1U * t27);
    t29 = (0 + t28);
    t19 = (t23 + t29);
    t30 = *((unsigned char *)t19);
    t31 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t30);
    t32 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t22, t31);
    t33 = (t0 + 15848);
    t34 = (t33 + 56U);
    t35 = *((char **)t34);
    t36 = (t35 + 56U);
    t37 = *((char **)t36);
    *((unsigned char *)t37) = t32;
    xsi_driver_first_trans_fast(t33);
    xsi_set_current_line(89, ng0);
    t1 = (t0 + 6872U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t1 = (t0 + 5752U);
    t4 = *((char **)t1);
    t5 = *((unsigned char *)t4);
    t1 = (t0 + 7192U);
    t6 = *((char **)t1);
    t1 = (t0 + 5112U);
    t8 = *((char **)t1);
    t10 = *((int *)t8);
    t11 = (t10 - 7);
    t12 = (t11 * -1);
    xsi_vhdl_check_range_of_index(7, 0, -1, t10);
    t13 = (1U * t12);
    t14 = (0 + t13);
    t1 = (t6 + t14);
    t7 = *((unsigned char *)t1);
    t15 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t7);
    t16 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t5, t15);
    t17 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t2, t16);
    t9 = (t0 + 5752U);
    t19 = *((char **)t9);
    t18 = *((unsigned char *)t19);
    t21 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t17, t18);
    t9 = (t0 + 7352U);
    t20 = *((char **)t9);
    t9 = (t0 + 5112U);
    t23 = *((char **)t9);
    t25 = *((int *)t23);
    t26 = (t25 - 7);
    t27 = (t26 * -1);
    xsi_vhdl_check_range_of_index(7, 0, -1, t25);
    t28 = (1U * t27);
    t29 = (0 + t28);
    t9 = (t20 + t29);
    t22 = *((unsigned char *)t9);
    t30 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t22);
    t31 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t21, t30);
    t24 = (t0 + 15912);
    t33 = (t24 + 56U);
    t34 = *((char **)t33);
    t35 = (t34 + 56U);
    t36 = *((char **)t35);
    *((unsigned char *)t36) = t31;
    xsi_driver_first_trans_fast(t24);
    goto LAB3;

}

static void work_a_2748367590_1516540902_p_9(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    int t7;
    char *t8;
    int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;

LAB0:    xsi_set_current_line(95, ng0);
    t2 = (t0 + 5752U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = (t4 == (unsigned char)3);
    if (t5 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB3;

LAB4:
LAB8:    t15 = (t0 + 15976);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    *((unsigned char *)t19) = (unsigned char)2;
    xsi_driver_first_trans_delta(t15, 0U, 1, 0LL);

LAB2:    t20 = (t0 + 15112);
    *((int *)t20) = 1;

LAB1:    return;
LAB3:    t2 = (t0 + 15976);
    t11 = (t2 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)3;
    xsi_driver_first_trans_delta(t2, 0U, 1, 0LL);
    goto LAB2;

LAB5:    t2 = (t0 + 5112U);
    t6 = *((char **)t2);
    t7 = *((int *)t6);
    t2 = (t0 + 8128U);
    t8 = *((char **)t2);
    t9 = *((int *)t8);
    t10 = (t7 == t9);
    t1 = t10;
    goto LAB7;

LAB9:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_10(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    int t7;
    char *t8;
    int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;

LAB0:    xsi_set_current_line(95, ng0);
    t2 = (t0 + 5752U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = (t4 == (unsigned char)3);
    if (t5 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB3;

LAB4:
LAB8:    t15 = (t0 + 16040);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    *((unsigned char *)t19) = (unsigned char)2;
    xsi_driver_first_trans_delta(t15, 1U, 1, 0LL);

LAB2:    t20 = (t0 + 15128);
    *((int *)t20) = 1;

LAB1:    return;
LAB3:    t2 = (t0 + 16040);
    t11 = (t2 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)3;
    xsi_driver_first_trans_delta(t2, 1U, 1, 0LL);
    goto LAB2;

LAB5:    t2 = (t0 + 5112U);
    t6 = *((char **)t2);
    t7 = *((int *)t6);
    t2 = (t0 + 8248U);
    t8 = *((char **)t2);
    t9 = *((int *)t8);
    t10 = (t7 == t9);
    t1 = t10;
    goto LAB7;

LAB9:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_11(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    int t7;
    char *t8;
    int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;

LAB0:    xsi_set_current_line(95, ng0);
    t2 = (t0 + 5752U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = (t4 == (unsigned char)3);
    if (t5 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB3;

LAB4:
LAB8:    t15 = (t0 + 16104);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    *((unsigned char *)t19) = (unsigned char)2;
    xsi_driver_first_trans_delta(t15, 2U, 1, 0LL);

LAB2:    t20 = (t0 + 15144);
    *((int *)t20) = 1;

LAB1:    return;
LAB3:    t2 = (t0 + 16104);
    t11 = (t2 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)3;
    xsi_driver_first_trans_delta(t2, 2U, 1, 0LL);
    goto LAB2;

LAB5:    t2 = (t0 + 5112U);
    t6 = *((char **)t2);
    t7 = *((int *)t6);
    t2 = (t0 + 8368U);
    t8 = *((char **)t2);
    t9 = *((int *)t8);
    t10 = (t7 == t9);
    t1 = t10;
    goto LAB7;

LAB9:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_12(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    int t7;
    char *t8;
    int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;

LAB0:    xsi_set_current_line(95, ng0);
    t2 = (t0 + 5752U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = (t4 == (unsigned char)3);
    if (t5 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB3;

LAB4:
LAB8:    t15 = (t0 + 16168);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    *((unsigned char *)t19) = (unsigned char)2;
    xsi_driver_first_trans_delta(t15, 3U, 1, 0LL);

LAB2:    t20 = (t0 + 15160);
    *((int *)t20) = 1;

LAB1:    return;
LAB3:    t2 = (t0 + 16168);
    t11 = (t2 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)3;
    xsi_driver_first_trans_delta(t2, 3U, 1, 0LL);
    goto LAB2;

LAB5:    t2 = (t0 + 5112U);
    t6 = *((char **)t2);
    t7 = *((int *)t6);
    t2 = (t0 + 8488U);
    t8 = *((char **)t2);
    t9 = *((int *)t8);
    t10 = (t7 == t9);
    t1 = t10;
    goto LAB7;

LAB9:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_13(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    int t7;
    char *t8;
    int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;

LAB0:    xsi_set_current_line(95, ng0);
    t2 = (t0 + 5752U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = (t4 == (unsigned char)3);
    if (t5 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB3;

LAB4:
LAB8:    t15 = (t0 + 16232);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    *((unsigned char *)t19) = (unsigned char)2;
    xsi_driver_first_trans_delta(t15, 4U, 1, 0LL);

LAB2:    t20 = (t0 + 15176);
    *((int *)t20) = 1;

LAB1:    return;
LAB3:    t2 = (t0 + 16232);
    t11 = (t2 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)3;
    xsi_driver_first_trans_delta(t2, 4U, 1, 0LL);
    goto LAB2;

LAB5:    t2 = (t0 + 5112U);
    t6 = *((char **)t2);
    t7 = *((int *)t6);
    t2 = (t0 + 8608U);
    t8 = *((char **)t2);
    t9 = *((int *)t8);
    t10 = (t7 == t9);
    t1 = t10;
    goto LAB7;

LAB9:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_14(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    int t7;
    char *t8;
    int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;

LAB0:    xsi_set_current_line(95, ng0);
    t2 = (t0 + 5752U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = (t4 == (unsigned char)3);
    if (t5 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB3;

LAB4:
LAB8:    t15 = (t0 + 16296);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    *((unsigned char *)t19) = (unsigned char)2;
    xsi_driver_first_trans_delta(t15, 5U, 1, 0LL);

LAB2:    t20 = (t0 + 15192);
    *((int *)t20) = 1;

LAB1:    return;
LAB3:    t2 = (t0 + 16296);
    t11 = (t2 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)3;
    xsi_driver_first_trans_delta(t2, 5U, 1, 0LL);
    goto LAB2;

LAB5:    t2 = (t0 + 5112U);
    t6 = *((char **)t2);
    t7 = *((int *)t6);
    t2 = (t0 + 8728U);
    t8 = *((char **)t2);
    t9 = *((int *)t8);
    t10 = (t7 == t9);
    t1 = t10;
    goto LAB7;

LAB9:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_15(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    int t7;
    char *t8;
    int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;

LAB0:    xsi_set_current_line(95, ng0);
    t2 = (t0 + 5752U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = (t4 == (unsigned char)3);
    if (t5 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB3;

LAB4:
LAB8:    t15 = (t0 + 16360);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    *((unsigned char *)t19) = (unsigned char)2;
    xsi_driver_first_trans_delta(t15, 6U, 1, 0LL);

LAB2:    t20 = (t0 + 15208);
    *((int *)t20) = 1;

LAB1:    return;
LAB3:    t2 = (t0 + 16360);
    t11 = (t2 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)3;
    xsi_driver_first_trans_delta(t2, 6U, 1, 0LL);
    goto LAB2;

LAB5:    t2 = (t0 + 5112U);
    t6 = *((char **)t2);
    t7 = *((int *)t6);
    t2 = (t0 + 8848U);
    t8 = *((char **)t2);
    t9 = *((int *)t8);
    t10 = (t7 == t9);
    t1 = t10;
    goto LAB7;

LAB9:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_16(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    int t7;
    char *t8;
    int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;

LAB0:    xsi_set_current_line(95, ng0);
    t2 = (t0 + 5752U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = (t4 == (unsigned char)3);
    if (t5 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB3;

LAB4:
LAB8:    t15 = (t0 + 16424);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    *((unsigned char *)t19) = (unsigned char)2;
    xsi_driver_first_trans_delta(t15, 7U, 1, 0LL);

LAB2:    t20 = (t0 + 15224);
    *((int *)t20) = 1;

LAB1:    return;
LAB3:    t2 = (t0 + 16424);
    t11 = (t2 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)3;
    xsi_driver_first_trans_delta(t2, 7U, 1, 0LL);
    goto LAB2;

LAB5:    t2 = (t0 + 5112U);
    t6 = *((char **)t2);
    t7 = *((int *)t6);
    t2 = (t0 + 8968U);
    t8 = *((char **)t2);
    t9 = *((int *)t8);
    t10 = (t7 == t9);
    t1 = t10;
    goto LAB7;

LAB9:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_17(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    int t7;
    int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    int t20;
    int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;
    char *t29;
    char *t30;

LAB0:    xsi_set_current_line(111, ng0);
    t1 = (t0 + 5592U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB3;

LAB4:
LAB5:    t17 = (t0 + 6072U);
    t18 = *((char **)t17);
    t17 = (t0 + 5112U);
    t19 = *((char **)t17);
    t20 = *((int *)t19);
    t21 = (t20 - 7);
    t22 = (t21 * -1);
    xsi_vhdl_check_range_of_index(7, 0, -1, t20);
    t23 = (32U * t22);
    t24 = (0 + t23);
    t17 = (t18 + t24);
    t25 = (t0 + 16488);
    t26 = (t25 + 56U);
    t27 = *((char **)t26);
    t28 = (t27 + 56U);
    t29 = *((char **)t28);
    memcpy(t29, t17, 32U);
    xsi_driver_first_trans_delta(t25, 0U, 32U, 0LL);

LAB2:    t30 = (t0 + 15240);
    *((int *)t30) = 1;

LAB1:    return;
LAB3:    t1 = (t0 + 5912U);
    t5 = *((char **)t1);
    t1 = (t0 + 4952U);
    t6 = *((char **)t1);
    t7 = *((int *)t6);
    t8 = (t7 - 1);
    t9 = (t8 * -1);
    xsi_vhdl_check_range_of_index(1, 0, -1, t7);
    t10 = (32U * t9);
    t11 = (0 + t10);
    t1 = (t5 + t11);
    t12 = (t0 + 16488);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 32U);
    xsi_driver_first_trans_delta(t12, 0U, 32U, 0LL);
    goto LAB2;

LAB6:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_18(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    int t6;
    int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned char t11;
    unsigned char t12;
    char *t13;
    char *t14;
    unsigned char t15;
    unsigned char t16;
    char *t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    unsigned char t21;
    char *t22;
    char *t23;
    int t24;
    int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned char t29;
    unsigned char t30;
    char *t31;
    char *t32;
    unsigned char t33;
    unsigned char t34;
    unsigned char t35;
    char *t36;
    char *t37;
    char *t38;
    char *t39;
    char *t40;

LAB0:    xsi_set_current_line(113, ng0);

LAB3:    t1 = (t0 + 5432U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 6712U);
    t4 = *((char **)t1);
    t1 = (t0 + 4952U);
    t5 = *((char **)t1);
    t6 = *((int *)t5);
    t7 = (t6 - 1);
    t8 = (t7 * -1);
    xsi_vhdl_check_range_of_index(1, 0, -1, t6);
    t9 = (1U * t8);
    t10 = (0 + t9);
    t1 = (t4 + t10);
    t11 = *((unsigned char *)t1);
    t12 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t3, t11);
    t13 = (t0 + 6232U);
    t14 = *((char **)t13);
    t15 = *((unsigned char *)t14);
    t16 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t12, t15);
    t13 = (t0 + 5592U);
    t17 = *((char **)t13);
    t18 = *((unsigned char *)t17);
    t19 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t16, t18);
    t13 = (t0 + 5752U);
    t20 = *((char **)t13);
    t21 = *((unsigned char *)t20);
    t13 = (t0 + 7352U);
    t22 = *((char **)t13);
    t13 = (t0 + 5112U);
    t23 = *((char **)t13);
    t24 = *((int *)t23);
    t25 = (t24 - 7);
    t26 = (t25 * -1);
    xsi_vhdl_check_range_of_index(7, 0, -1, t24);
    t27 = (1U * t26);
    t28 = (0 + t27);
    t13 = (t22 + t28);
    t29 = *((unsigned char *)t13);
    t30 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t21, t29);
    t31 = (t0 + 6872U);
    t32 = *((char **)t31);
    t33 = *((unsigned char *)t32);
    t34 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t30, t33);
    t35 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t19, t34);
    t31 = (t0 + 16552);
    t36 = (t31 + 56U);
    t37 = *((char **)t36);
    t38 = (t37 + 56U);
    t39 = *((char **)t38);
    *((unsigned char *)t39) = t35;
    xsi_driver_first_trans_delta(t31, 32U, 1, 0LL);

LAB2:    t40 = (t0 + 15256);
    *((int *)t40) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_2748367590_1516540902_p_19(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;

LAB0:    xsi_set_current_line(114, ng0);

LAB3:    t1 = (t0 + 16616);
    t2 = (t1 + 56U);
    t3 = *((char **)t2);
    t4 = (t3 + 56U);
    t5 = *((char **)t4);
    *((unsigned char *)t5) = (unsigned char)2;
    xsi_driver_first_trans_delta(t1, 33U, 1, 0LL);

LAB2:
LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_2748367590_1516540902_init()
{
	static char *pe[] = {(void *)work_a_2748367590_1516540902_p_0,(void *)work_a_2748367590_1516540902_p_1,(void *)work_a_2748367590_1516540902_p_2,(void *)work_a_2748367590_1516540902_p_3,(void *)work_a_2748367590_1516540902_p_4,(void *)work_a_2748367590_1516540902_p_5,(void *)work_a_2748367590_1516540902_p_6,(void *)work_a_2748367590_1516540902_p_7,(void *)work_a_2748367590_1516540902_p_8,(void *)work_a_2748367590_1516540902_p_9,(void *)work_a_2748367590_1516540902_p_10,(void *)work_a_2748367590_1516540902_p_11,(void *)work_a_2748367590_1516540902_p_12,(void *)work_a_2748367590_1516540902_p_13,(void *)work_a_2748367590_1516540902_p_14,(void *)work_a_2748367590_1516540902_p_15,(void *)work_a_2748367590_1516540902_p_16,(void *)work_a_2748367590_1516540902_p_17,(void *)work_a_2748367590_1516540902_p_18,(void *)work_a_2748367590_1516540902_p_19};
	xsi_register_didat("work_a_2748367590_1516540902", "isim/ipb_syncreg_tb_isim_beh.exe.sim/work/a_2748367590_1516540902.didat");
	xsi_register_executes(pe);
}
