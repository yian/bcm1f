# BCM1F Software

This repository is a collection of online tools for BCM1F operations and lab tests.

## Modules

+ DEPRECATED! [AOH_bias_scan](https://gitlab.cern.ch/bril/bcm1f_sw/tree/master/AOH_bias_scan) performs the AOH bias scan for the VME and uTCA systems. Have a look at the [instructions](https://gitlab.cern.ch/bril/bcm1f_sw/wikis/aoh-bias-scan) in order to run it.
+ [slow_control](https://gitlab.cern.ch/bril/bcm1f_sw/tree/master/slow_control) is the new, Python based slow control, compatible with the old and new slow hub chip. Check the [README](https://gitlab.cern.ch/bril/bcm1f_sw/blob/master/slow_control/README.md).
## Contributions

This is the official repository, dedicated to production ready code for operations. Please do not place any private files and experimental code here. For this you can for example create a [branch](https://docs.gitlab.com/ee/gitlab-basics/create-branch.html) of this repository or create a repository on your own Gitlab space.

Also, it is mandatory to **document** your code and module in a way that makes it easy for anyone to *understand* and *use* it. Documentation in the repository is written in [markdown](https://guides.github.com/features/mastering-markdown/), which is very simple and quick to learn.

Learn git! Here is the best [link](https://try.github.io) to a git tutorial - remember, it's not centralized but distributed! And don't forget to write meaningful commit messages...
