#!/bin/bash

# source ./uTCA_thresholds.sh

usage() {
    echo 'Usage: ./runCshapeTest.sh <CshapeID> <gain(0-3)> <laserBias(0x0-0x3f)> <withPLT|noPLT> <highTP|lowTP|noTP> <HVvalue(0-1000)|customHV> <withSource|noSource> <iterations> <superdirectory>'
}

if test $# -ne 9
then
    echo 'Wrong number of arguments in run_uTCA.sh!'
    usage 
    exit 1
else
    CSHAPE=$1
    GAIN=$2
    LASER_BIAS=$3
    WITH_PLT=$4
    WITH_TEST_PULSE=$5
    HV_VALUE=$6
    WITH_SOURCE=$7
    ITERATIONS=$8
    SUPER_DIRECTORY_FULL_PATH=$9
fi

DIRNAME=${SUPER_DIRECTORY_FULL_PATH}/uTCA

cd ~/sw

for i in 1 2 3 7 8
do
    echo "Board ${i}"
    bin/bias_scan_tool mch-s1e10-05-01 1 bcm1f.crate1.amc$i ${ITERATIONS} ${DIRNAME}/c1a${i}_${LASER_BIAS}
    bin/bias_scan_tool mch-s1e10-14-01 1 bcm1f.crate2.amc$i ${ITERATIONS} ${DIRNAME}/c2a${i}_${LASER_BIAS}
done
cd -
