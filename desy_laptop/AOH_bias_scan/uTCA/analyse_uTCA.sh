#!/bin/bash

if test $# -ne 4
then
    echo 'Wrong number of arguments in analyse_uTCA.sh!'
    echo 'Usage: ./analyse_uTCA.sh <super_directory> <super_directory_full_path>'
    exit 1
else
    SUPER_DIRECTORY=$1
    SUPER_DIRECTORY_FULL_PATH=$2
    MIN=$3
    MAX=$4
fi

# python ~/AOH_bias_scan/uTCA/analyse_uTCA.py ${SUPER_DIRECTORY_FULL_PATH}/uTCA/ ${MIN} ${MAX}
python ./uTCA/analyse_uTCA.py ${SUPER_DIRECTORY_FULL_PATH}/uTCA/ ${MIN} ${MAX}
# convert "${SUPER_DIRECTORY_FULL_PATH}/uTCA/*.{png,}" Results/uTCA_${SUPER_DIRECTORY}.pdf
convert "${SUPER_DIRECTORY_FULL_PATH}/uTCA/*.{pdf,}" Results/uTCA_${SUPER_DIRECTORY}.pdf


# cd ${SUPER_DIRECTORY_FULL_PATH}/uTCA
# ls -d */ | while read DIRNAME
# do
#     ls *.csv | while read FILENAME
#     do
#         # Get laser bias value from filename
#         LASER_BIAS=$( echo "${DIRNAME}" |awk --re-interval '{match($0,/(.{0})laserbias(.{4})/,a);print a[2]}' )
#         python analyse_uTCA.py ${DIRNAME}/${FILENAME}
#     done;
# done;
