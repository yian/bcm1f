import matplotlib
import csv
import numpy
from matplotlib import pyplot as plt
import sys
import os
import collections

path = sys.argv[1]
# bias_min = int( sys.argv[2], 0 )
# bias_max = int( sys.argv[3], 0 )

# data = { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] }

dictCA = collections.OrderedDict()
dictCA = { "CH_A": {'c1a1': '+ Z NEAR, Pos F, Ch ID 11', 'c1a2': '+ Z NEAR, Pos A, Ch ID 1', 'c1a3': '+ Z FAR, Pos A, Ch ID 13', 'c1a7': '+ Z NEAR, Pos E, Ch ID 9', 'c1a8': '+ Z NEAR, Pos B, Ch ID 3', 'c2a1': '- Z NEAR, Pos F, Ch ID 35', 'c2a2': '- Z NEAR, Pos A, Ch ID 25', 'c2a3': '- Z FAR, Pos A, Ch ID 37', 'c2a7': '- Z NEAR, Pos E, Ch ID 33', 'c2a8': '- Z NEAR, Pos B, Ch ID 27'},
           "CH_B": {'c1a1': '+ Z NEAR, Pos F, Ch ID 12', 'c1a2': '+ Z NEAR, Pos A, Ch ID 2', 'c1a3': '+ Z FAR, Pos A, Ch ID 14', 'c1a7': '+ Z NEAR, Pos E, Ch ID 10', 'c1a8': '+ Z NEAR, Pos B, Ch ID 4', 'c2a1': '- Z NEAR, Pos F, Ch ID 36', 'c2a2': '- Z NEAR, Pos A, Ch ID 26', 'c2a3': '- Z FAR, Pos A, Ch ID 38', 'c2a7': '- Z NEAR, Pos E, Ch ID 34', 'c2a8': '- Z NEAR, Pos B, Ch ID 28'},
           "CH_C": {'c1a1': '+ Z FAR, Pos F, Ch ID 23', 'c1a2': '+ Z NEAR, Pos C, Ch ID 5', 'c1a3': '+ Z FAR, Pos C, Ch ID 17', 'c1a7': 'non existent', 'c1a8': '+ Z NEAR, Pos B, Ch ID 15', 'c2a1': '- Z FAR, Pos F, Ch ID 47', 'c2a2': '- Z NEAR, Pos C, Ch ID 29', 'c2a3': '- Z FAR, Pos C, Ch ID 41', 'c2a7': 'non existent', 'c2a8': '- Z FAR, Pos B, Ch ID 39'},
           "CH_D": {'c1a1': '+ Z FAR, Pos F, Ch ID 24', 'c1a2': '+ Z NEAR, Pos C, Ch ID 6', 'c1a3': '+ Z FAR, Pos C, Ch ID 18', 'c1a7': 'non existent', 'c1a8': '+ Z NEAR, Pos D, Ch ID 20', 'c2a1': '- Z FAR, Pos F, Ch ID 48', 'c2a2': '- Z NEAR, Pos C, Ch ID 30', 'c2a3': '- Z FAR, Pos C, Ch ID 42', 'c2a7': 'non existent', 'c2a8': '- Z FAR, Pos E, Ch ID 46'} }
means_all = collections.OrderedDict()
means_all = { "c1a1": { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] },
              "c1a2": { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] },
              "c1a3": { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] },
              "c1a7": { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] },
              "c1a8": { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] },
              "c2a1": { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] },
              "c2a2": { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] },
              "c2a3": { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] },
              "c2a7": { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] },
              "c2a8": { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] } }
bias = []

# Open every file in directory
for filename in sorted( os.listdir( path ) ):
  if filename.endswith(".csv"):
  # if filename.startswith("c1a3") and filename.endswith(".csv"):
    CrateAmc = filename.partition('_')[0]
    # bias_str = filename.partition('_')[2].partition('.')[0]
    # bias = int( filename.partition('_')[2].partition('.')[0], 10 )
    f = open( path + filename )
    try:
      for row in csv.reader( f ):
        data[row[0]].append( numpy.array( map( int, row[1:-1] ) ) )
      # DUMB PEAKFINDER (looking for TP)
      peaks = { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] }
      for ch in data:
        for row in range( len( data[ch] ) ):
          peak = 0
          for i in range( 0, 10000 ): # 3000 5000
            if data[ch][row][i] > peak:
              peak = data[ch][row][i]
          peaks[ch].append( peak )
      means = { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] }
      for ch in data:
        means[ch].append( sum( peaks[ch], 0.0 ) / len( peaks[ch] ) )
      for ch in data:
        means_all[CrateAmc][ch].extend( means[ch] )
    finally:
      f.close()

# for filename in sorted( os.listdir( path ) ):
#   if filename.endswith(".csv"):
#     CrateAmc = filename.partition('_')[0]
#     bias_str = filename.partition('_')[2].partition('.')[0]
#     bias = int( filename.partition('_')[2].partition('.')[0], 10 )
#     data = { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] }
#     f = open( path + filename )
#     try:
#       for row in csv.reader( f ):
#         data[row[0]].append( numpy.array( map( int, row[1:-1] ) ) )
      
#     finally:
#       f.close()

# PLOTS
for amc in sorted(means_all):
  fig, axs = plt.subplots(2, 2, figsize=(40, 20), facecolor='w', edgecolor='k' )
  axs = axs.ravel()
  i = 0
  for ch in means_all[amc]:
    axs[i].plot( means_all[amc][ch] )
    axs[i].set_title( dictCA[ch][amc] + "\n(" + amc + "_" + ch + ")", fontsize= 25)
    axs[i].set_xlabel('BIAS', fontsize = 20)
    axs[i].set_ylabel('TP_PEAK', fontsize = 20)
    # axs[i].set_xlim( [bias_min, bias_max] )
    axs[i].set_ylim( [120, 250] )
    fig.savefig( path + "/" + amc + '.pdf' )
    i += 1
  fig.clf()

print "uTCA analysis DONE."


# f = open( path + "c1a1_01.csv" )
# for row in csv.reader( f ):
#   data[row[0]].append( numpy.array( map( int, row[1:-1] ) ) )
# peaks = { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] }
# for ch in data:
#   print ch
#   for row in range( len( data[ch] ) ):
#     # print row
#     peak = 0
#     for i in range( 0, 65535 ): # 3000 5000
#       if data[ch][row][i] > peak:
#         peak = data[ch][row][i]
#       # if peak > 135
#     peaks[ch].append( peak )
#     # print peaks
#   print peaks
# means = { "CH_A": [], "CH_B": [], "CH_C": [], "CH_D": [] }
# for ch in data:
#   means[ch].append( sum( peaks[ch], 0.0 ) / len( peaks[ch] ) )
# print means

# f = open( path + "c1a1_20.csv" )
# for row in csv.reader( f ):
#   data[row[0]].append( numpy.array( map( int, row[1:-1] ) ) )

# plt.plot( data["CH_A"][1] )
# plt.show()