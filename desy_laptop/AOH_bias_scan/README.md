# AOH Bias Scan Tool

## How to Run

From cmsusr log in on brilvme4 (necessary for ADC readout):

`ssh brilvme4`  

Change to brilpro directory that contains the necessary files:

`cd /brilpro/bcm1f/AOH_bias_scan`

Become *brilpro*:

`sudo –u brilpro bash –l`

Set the environment:

`source ./env.sh`

You are now ready to start the scan:

`. ./run_scan.sh <VME|uTCA|ALL> <Start(min 0)> <Stop(max 63)>`

As arguments pass the system you want to scan(VME/uTCA/ALL). Specify also the *Start* and *Stop* bias values (min 0, max 63).   Recommended to run the full scan is start: 0 and stop: 63.  
Example for running VME scan:

`. ./run_scan.sh VME 0 63`

The scan now sets the bias values and saves ADC (raw) data. The data gets saved in /brildata/bcm1futca/AOHscan/<VME/uTCA>. The scan_name is denoted by the date and time as well as the used gain and system. At the end of the scan the script resets the bias values automatically with the current values set in *configureAllAOHsWithSeparateValues.sh*.

IF THE SCRIPT CRASHES THE AOH CONFIGURATIO IS MOST LIKELY WRONG, **RESET TO DEFAULT** WITH: `. ./resetAOHs.sh`

## Analysing the Data

Make sure X-forwarding is enabled when logging into the respective machine (e.g. `ssh –XY brildev1`).  
Run as brilpro, but again X-forwarding needs to be activated, so use xsudo indstead of sudo.

`xsudo –u brilpro bash –l`

From folder AOH_bias_scan run:

`. ./run_plot.sh <scanname> <VME|uTCA>`

For example to analyse the VME scan from 02/05/2018 at 15h50:

`./run_plot.sh 2018-05-02_15-50_gain3_systemsVME VME`

In folder results this creates a .pdf file which shows the plots for every single channel, indicating the suggested new value and the currently set value in configureAllAOHsWithSeparateValues.sh. Additionally, a .csv file is created with the recommended values which can be copied to configureAllAOHsWithSeparateValues.sh. Check that the automatically generated values are good by looking at all scan curves. 