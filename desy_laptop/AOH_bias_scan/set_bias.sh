#!/bin/bash
# trap "" HUP

#######################################
# LOOPING OVER BIAS VALUES
#######################################

BIAS=$1

printf -v BIAS_HEX '%02x' ${BIAS}
printf -v BIAS_DEC '%02d' ${BIAS}
BIAS_HEX="0x${BIAS_HEX}"
echo "Starting bias value ${BIAS_HEX}"

for PORT in 0 1 2 3
do
    for LASER in L M R
    do
        # /brilpro/bcm1f/CshapeTesting/bcm1f_slow_control_SVN/bin/slow_control_direct_choice AOH ${PORT} ${LASER} ${BIAS_HEX} ${GAIN} ALL quiet
    done
done
