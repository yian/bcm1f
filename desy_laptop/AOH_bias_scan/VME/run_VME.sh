#!/bin/bash

source /etc/init.d/functions
source ./VME/sw/configure.sh
echo $pwd
DAQ_VERSION="DAQ_v1721"
PROGNAME=operateBCM1F

usage() {
    echo 'Usage: ./run_VME.sh <laserBias(0x0-0x3f)> <superdirectory>'
}

if test $# -ne 2
then
    echo 'Wrong number of arguments in run_VME.sh!'
    usage 
    exit 1
else
    LASER_BIAS=$1
    SUPER_DIRECTORY=$2
    SYSTEM=$3
fi

PATH=${SUPER_DIRECTORY}/VME/

## Start
echo "Starting ${PROGNAME}"
./VME/sw/${PROGNAME} ${LASER_BIAS} ${PATH}
