import matplotlib
import csv
import numpy
from matplotlib import pyplot as plt
import sys
import os

path = sys.argv[1]

def VME_BrdCh():
  BrdCh = {   "BRD_0": { "CH_0": [], "CH_1": [], "CH_2": [], "CH_3": [], "CH_4": [], "CH_5": [], "CH_6": [], "CH_7": [] },
              "BRD_1": { "CH_0": [], "CH_1": [], "CH_2": [], "CH_3": [], "CH_4": [], "CH_5": [], "CH_6": [], "CH_7": [] },
              "BRD_2": { "CH_0": [], "CH_1": [], "CH_2": [], "CH_3": [], "CH_4": [], "CH_5": [], "CH_6": [], "CH_7": [] },
              "BRD_3": { "CH_0": [], "CH_1": [], "CH_2": [], "CH_3": [], "CH_4": [], "CH_5": [], "CH_6": [], "CH_7": [] },
              "BRD_4": { "CH_0": [], "CH_1": [], "CH_2": [], "CH_3": [], "CH_4": [], "CH_5": [], "CH_6": [], "CH_7": [] },
              "BRD_5": { "CH_0": [], "CH_1": [], "CH_2": [], "CH_3": [], "CH_4": [], "CH_5": [], "CH_6": [], "CH_7": [] } }
  return BrdCh

BrdCh = VME_BrdCh()
bias = []

DictBrdCh = { "BRD_0": {  "CH_0": '+ Z NEAR, ChID 1', "CH_1": '+ Z NEAR, ChID 2', "CH_2": '+ Z NEAR, ChID 3', "CH_3": '+ Z NEAR, ChID 4',
                          "CH_4": '+ Z NEAR, ChID 5', "CH_5": '+ Z NEAR, ChID 6', "CH_6": '+ Z NEAR, ChID 7', "CH_7": '+ Z NEAR, ChID 8' },
              "BRD_1": {  "CH_0": '+ Z NEAR, ChID 9', "CH_1": '+ Z NEAR, ChID 10', "CH_2": '+ Z NEAR, ChID 11', "CH_3": '+ Z NEAR, ChID 12',
                          "CH_4": '+ Z FAR, ChID 13', "CH_5": '+ Z FAR, ChID 14', "CH_6": '+ Z FAR, ChID 15', "CH_7": '+ Z FAR, ChID 16' },
              "BRD_2": {  "CH_0": '+ Z FAR, ChID 17', "CH_1": '+ Z FAR, ChID 18', "CH_2": '+ Z FAR, ChID 19', "CH_3": '+ Z FAR, ChID 20',
                          "CH_4": '+ Z FAR, ChID 21', "CH_5": '+ Z FAR, ChID 22', "CH_6": '+ Z FAR, ChID 23', "CH_7": '+ Z FAR, ChID 24' },
              "BRD_3": {  "CH_0": '- Z NEAR, ChID 25', "CH_1": '- Z NEAR, ChID 26', "CH_2": '- Z NEAR, ChID 27', "CH_3": '- Z NEAR, ChID 28',
                          "CH_4": '- Z NEAR, ChID 29', "CH_5": '- Z NEAR, ChID 30', "CH_6": '- Z NEAR, ChID 31', "CH_7": '- Z NEAR, ChID 32' },
              "BRD_4": {  "CH_0": '- Z NEAR, ChID 33', "CH_1": '- Z NEAR, ChID 34', "CH_2": '- Z NEAR, ChID 35', "CH_3": '- Z NEAR, ChID 36',
                          "CH_4": '- Z FAR, ChID 37', "CH_5": '- Z FAR, ChID 38', "CH_6": '- Z FAR, ChID 39', "CH_7": '- Z FAR, ChID 40' },
              "BRD_5": {  "CH_0": '- Z FAR, ChID 41', "CH_1": '- Z FAR, ChID 42', "CH_2": '- Z FAR, ChID 43', "CH_3": '- Z FAR, ChID 44',
                          "CH_4": '- Z FAR, ChID 45', "CH_5": '- Z FAR, ChID 46', "CH_6": '- Z FAR, ChID 47', "CH_7": '- Z FAR, ChID 48' } }

new_bias = {  "BRD_0": { "CH_0": '', "CH_1": '', "CH_2": '', "CH_3": '', "CH_4": '', "CH_5": '', "CH_6": '', "CH_7": '' },
              "BRD_1": { "CH_0": '', "CH_1": '', "CH_2": '', "CH_3": '', "CH_4": '', "CH_5": '', "CH_6": '', "CH_7": '' },
              "BRD_2": { "CH_0": '', "CH_1": '', "CH_2": '', "CH_3": '', "CH_4": '', "CH_5": '', "CH_6": '', "CH_7": '' },
              "BRD_3": { "CH_0": '', "CH_1": '', "CH_2": '', "CH_3": '', "CH_4": '', "CH_5": '', "CH_6": '', "CH_7": '' },
              "BRD_4": { "CH_0": '', "CH_1": '', "CH_2": '', "CH_3": '', "CH_4": '', "CH_5": '', "CH_6": '', "CH_7": '' },
              "BRD_5": { "CH_0": '', "CH_1": '', "CH_2": '', "CH_3": '', "CH_4": '', "CH_5": '', "CH_6": '', "CH_7": '' } }

# parse values in configureAllAOHsWithSeparateValues.sh
laser_bias = [0] * 48
print "Parsing current bias values..."
with open("/brilpro/bcm1f/Slow_Control/configureAllAOHsWithSeparateValues.sh") as fl:
  try:
    for line in fl:
      if line.startswith("LASER_BIAS"):
        laser_bias[ int( line.partition(']')[0].partition('LASER_BIAS[')[2], 10 ) ] = int( line.partition('=')[2], 16 )
  finally:
    fl.close()

## Read the data from files
print "Analysing scan data..."
for filename in sorted( os.listdir( path ) ):
  if filename.startswith("bias") and filename.endswith(".csv"):
    print filename
    bias_val = int( filename.partition('_')[2].partition('.')[0], 10 )
    bias.append( bias_val )
    data = VME_BrdCh()
    f = open( path + filename, 'r' )
    n = 10
    try:
      for row in csv.reader( f ):
        data[row[0]][row[1]].append( numpy.array( map( int, row[3:n+3] ) ) )
      for brd in data:
        for ch in data[brd]:
          BrdCh[brd][ch].append( numpy.mean( data[brd][ch][:n] ) )
    finally:
      f.close()

# Find the location of the beginning of the linear region of the laser diode using second derivative:
for brd in BrdCh:
  for ch in BrdCh[brd]:
    if BrdCh[brd][ch]:
      first_deriv = numpy.gradient( BrdCh[brd][ch], 10 )
      second_deriv = numpy.gradient( first_deriv , 4 )
      ## get the bias value of the end of the plateau:
      min_val = numpy.argmin( second_deriv ) # reference value
      new_bias[brd][ch] = min_val + 3

print "Plotting results..."

for brd in BrdCh:
  for ch in BrdCh[brd]:

    fig, ax = plt.subplots( nrows = 1, ncols = 1 )
    ax.plot( sorted(bias), BrdCh[brd][ch], 'b', label="Scan" )

    # plot new bias
    if new_bias[brd][ch] < len(BrdCh[brd][ch]):
      plt.plot( new_bias[brd][ch] , BrdCh[brd][ch][ new_bias[brd][ch] ], "g^", markersize=6, label="Suggested bias" )
      # plot old bias
    old_bias_idx = int( DictBrdCh[brd][ch].partition("ChID ")[2], 10 ) - 1
    if laser_bias[ old_bias_idx ] < len(BrdCh[brd][ch]):
      plt.plot( laser_bias[ old_bias_idx ] , BrdCh[brd][ch][ laser_bias[ old_bias_idx ] ], "rv", markersize=6, label="Current bias" )

    ax.set_title( DictBrdCh[brd][ch] + "\n" + "(" + brd + "_" + ch + ")", fontsize = 16 )
    ax.set_xlabel( 'Bias value (decimal)', fontsize = 12 )
    ax.set_ylabel( 'Baseline (ADC counts)', fontsize = 12 )
    ax.set_xlim( [0, 63] )
    ax.set_ylim( [0, 200] )
    ax.legend( numpoints=1 )
    fig.savefig( DictBrdCh[brd][ch][-2:]+'.png' )
    plt.close( fig )

# File output

fo_new = open( "recommendationVME.csv", 'w' )
try:
  # fo_new.write( "# " + path + "\n" )
  i = 0
  for brd in new_bias:
    for ch in new_bias[brd]:
      fo_new.write( "LASER_BIAS[" + str(i) + "]=")
      fo_new.write( str( hex( new_bias[brd][ch] ).rstrip('L') ) + "\n" )
      i += 1
finally:
  fo_new.close()

# fo_new = open( "history.csv", 'w' )
# try:
#   fo_new.write( path + "\n" )
#   for brd in new_bias:
#     for ch in new_bias[brd]:
#       fo_new.write( brd + ",")
#       fo_new.write( ch + ",")
#       fo_new.write( str( new_bias[brd][ch] ) + "\n" )
# finally:
#   fo_new.close()
