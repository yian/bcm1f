#include <fstream>
#include <iostream>
#include <string.h>

#include "adc_v1721_caen.hh"

#define BLTSIZE 4096  // Words (32bit) per Block Transfer; Recom.: 16kB, Maximum: 60kB!
#define MAX_N_BOARDS 6

ADC_V1721_CAEN* adc[MAX_N_BOARDS];     
unsigned int n_ADCs = 6; // ADC handle
unsigned long baseaddress[MAX_N_BOARDS]; // 1 : base address (with default value)
short vme_link=2; // 2 : PCI card number (starting from 0)
short board_no[MAX_N_BOARDS] = {0,1,2,3,4,5}; // 3 : board number within a daisy-chain
int bufforg=0x0a; // 6 : buffer organisation
int customsize=32; // 7 : custom size
int posttrgno=16; // 8 : post trigger number
int chmask=0xff; // 5 : active channel mask
//------ Trigger mode and thresholds ---------------------------------------------------
int trigger_mode=0; // 9 : 0=ext, 1=SW, 2=abs, 3=rel(counts), 4=rel(sigmas)
double trg_thres_ch[NCH]={1,1,1,1,1,1,1,1}; // 10-17: thresholds
unsigned int time_ou_thres_ch[NCH]={2,2,2,2,2,2,2,2}; // 18-25: minimal time over/under threshold
int coinc_level=0; // software trigger coincidences; not selectable via the configuration file
//------ Data processing ---------------------------------------------------------------
bool STOPPING=false; // STOPPING flag
unsigned int TriggerCounter=0; // Bits 0 to 23 of the 3rd word in the event header
unsigned int TCOverflows=0; // Number of overflows of "Event Counter" (only 24 bits)
int Nsamples=0; // samples (bytes) per channel and event
int Nwords=0; // words (32 bit) per channel and event
unsigned long BLTdata[MAX_N_BOARDS][BLTSIZE]; // long, not int!!  // Array for Block Transfers from ADC buffer memory
unsigned int TotalData[MAX_N_BOARDS][0x400004]; // maximum event size (16MB + 16B header), each int element taking 4 samples
unsigned int Event = 0;
unsigned int board = 0;
unsigned int channel = 0;

//---------------------------------------------------------------------------
// Read out data from the ADC
//---------------------------------------------------------------------------
bool read_data(unsigned int nAdcBoard)
{
  int ret=0;
  int actual=0;
  unsigned int index=0;
  do { // read one event
    ret = adc[nAdcBoard]->Read_MBLT64_T(BLTdata[nAdcBoard], BLTSIZE*sizeof(int), &actual);
    memcpy(&TotalData[nAdcBoard][index], BLTdata[nAdcBoard], actual);
    index += actual/sizeof(int);
  } while (ret==0);

  if (ret == -1) 
  {
    if (TriggerCounter > (0xffffff & TotalData[nAdcBoard][2]))  TCOverflows++;
    TriggerCounter = 0xffffff & TotalData[nAdcBoard][2];
    return true;
  }
  return false;
}

//---------------------------------------------------------------------------
// Trigger source external
//---------------------------------------------------------------------------
int set_trigger_external() {
  int ret=0;
  for (unsigned int count = 0; count < n_ADCs; count++)
  {
    ret = adc[count]->Disable_SW_Trg_Src();
    if (ret<0)
    return ret;
  }
  for (unsigned int count = 0; count < n_ADCs; count++)
  {
    ret = adc[count]->Disable_Ch_Trg_Src(NCH);
    if (ret<0)
    return ret;
  }
  for (unsigned int count = 0; count < n_ADCs; count++)
  {
    ret = adc[count]->Enable_External_Trg_Src();
    if (ret<0)
    return ret;
  }
  std::cerr << " Trigger source set to external. \n\t--> Trigger signal must be provided!" << std::endl;
  return 0;
}

//---------------------------------------------------------------------------
// Self-trigger mode using absolute thresholds
//--------------------------------------------------------------------------- 2
int set_thresholds_absolute() {
  int ret=0;
  for (unsigned int count = 0; count < n_ADCs; count++)
  {
    ret = adc[count]->Disable_External_Trg_Src();
    if (ret<0)
    return ret;
  }
  for (unsigned int count = 0; count < n_ADCs; count++)
  {
    ret = adc[count]->Disable_SW_Trg_Src();
    if (ret<0)
    return ret;
  }
  for (unsigned int count = 0; count < n_ADCs; count++)
  {
    ret = adc[count]->Set_TRG_OUT_UNDER_Threshold();
    if (ret<0)
    return ret;
  }
  for (unsigned int count = 0; count < n_ADCs; count++)
  {
    if (chmask==0xff)
    {
      ret = adc[count]->Enable_Ch_Trg_Src(NCH);
      if (ret<0)
        return ret;
      for (int i=0; i<NCH; i++)
      {
        ret = adc[count]->Set_Ch_Trg_Threshold(i, (unsigned int)trg_thres_ch[i]);
        if (ret<0) break;
        ret = adc[count]->Set_DATANUMBER_OvrUndr_Thres(i, time_ou_thres_ch[i]);
        if (ret<0) break;
      }
    }
    else
    {
      for (int i=0; i<NCH; i++)
      {
        if ( (chmask & (1<<i)) == (1<<i) )
        {
          ret = adc[count]->Enable_Ch_Trg_Src(i);
          if (ret<0) break;
          ret = adc[count]->Set_Ch_Trg_Threshold(i, (unsigned int)trg_thres_ch[i]);
          if (ret<0) break;
          ret = adc[count]->Set_DATANUMBER_OvrUndr_Thres(i, time_ou_thres_ch[i]);
          if (ret<0) break;
        }
        else
        {
          ret = adc[count]->Disable_Ch_Trg_Src(i);
          if (ret<0) break;
        }
      }
    }
    if (ret<0)
      return ret;
  }
    
  for (unsigned int count = 0; count < n_ADCs; count++)
  {
    ret = adc[count]->Set_LocalCoincidenceLevel(coinc_level);
    if (ret<0)
    return ret;
  }
  // std::cerr << " Trigger thresholds successfully set. " << std::endl;
  return 0;
}


//===========================================================================
// Main Function
//===========================================================================
int main(int argc, char* argv[])
{
  baseaddress[0]=0x55550000;  //  # p5, SN 780, link0 
  baseaddress[1]=0x44440000;  //  # p5, SN 468, link1
  baseaddress[2]=0x33330000;  //  # p5, SN 109, link2
  baseaddress[3]=0x11110000;  //  # p5, SN 152, link3
  baseaddress[4]=0x66660000;  //  # p5, SN 317, link4
  baseaddress[5]=0x77770000;  //  # p5, SN 862, link5

  // Establish and configure the ADC
  int retval;
  for (unsigned int count = 0; count < n_ADCs; count++)
  {
    adc[count] = new ADC_V1721_CAEN(baseaddress[count], "ADC_V1721_CAEN", vme_link, board_no[count]);
    adc[count]->Set_ACQ_RUN_STOP();
  }

  for (unsigned int count = 0; count < n_ADCs; count++) 
  {
    adc[count]->Set_Count_ALL_Trg(); // count all triggers
    adc[count]->Set_FrontPanel_PatternMode(); // to see LVDS Pattern in header
    adc[count]->Set_Buffer_Organisation(bufforg); // see config file
    adc[count]->Set_Custom_Size(customsize); //        "
    adc[count]->Set_Post_Trg(posttrgno); //        "
    adc[count]->Set_BLT_Event_Number(1); // One event per block transfer
    // std::cerr << " ADC  " << count << std::endl;
    for (int i=0; i<NCH; i++) {
      if ( (chmask & (1<<i)) == (1<<i) ) {
        adc[count]->Enable_Ch(i);
        // std::cerr << " Enable Channel  " << i << std::endl;
      } else {
        adc[count]->Disable_Ch(i);
      }
    }
  }

  switch (trigger_mode)
  {
    case 0:
      retval = set_trigger_external();
      break;
    case 2:
      retval = set_thresholds_absolute();
      break;
    case 3:
    case 4:
    default:
      std::cerr << "The requested mode does not exist or is not yet implemented." << std::endl;
      retval = set_trigger_external();
      break;
  }
  if (retval<0)
  {
    std::cerr << " Could not set the requested trigger mode.\nPROGRAMME ABORTED!!!" << std::endl;
    exit(-1);
  }

  // Main read and write loop

  std::string biasval = argv[1];
  std::string DO_Path = argv[2];
  std::string filename = DO_Path + "bias_" + biasval + ".csv";
  std::ofstream savedata( filename );

  for (unsigned int count = 0; count < n_ADCs; count++)
  {
	  adc[count]->Set_ACQ_RUN_START();
  }

  bool ADC_comm = true;  
  int eventCount = 0;

  // read data
  retval = adc[0]->Wait_4_Event_unsleep(&STOPPING); 
  if (retval==0) 
  { // data available
    eventCount++;
    if (eventCount % 500 == 0) std::cout << "Event count is now " << eventCount << std::endl;
    for (unsigned int nAdcBoard = 0; nAdcBoard < n_ADCs; nAdcBoard++) { // looping through global variable
      ADC_comm = read_data(nAdcBoard); // Successful?
      if (ADC_comm)
      {
        // treat_data(nAdcBoard);
        int lchmask = (0xff & TotalData[nAdcBoard][1]);
        int NactiveChannels = 0;
        for (int i=0; i<NCH; i++) if ((lchmask & (1<<i)) == (1<<i))  NactiveChannels++;
        unsigned int DataSize = (TotalData[nAdcBoard][0] & 0xfffffff) - 4; // in words (32bit)
        if ( NactiveChannels == 0 || DataSize == 0 )
        {
            std::cerr << "ADC board # " << nAdcBoard << " doesn't have any data. Data treatment is terminated. Orbit clock could be missing." << std::endl; return -1;
        }
        Nwords = int( DataSize / NactiveChannels ); // words per channel
        Nsamples = Nwords * 4; // samples per channel
        std::cerr << " Nwords: " << Nwords << " Nsamples: " << Nsamples << std::endl;
        if ( Nsamples>= 2097152 )
        {
          std::cerr << "Nsamples in Data does not mach! Sholud be < 2097152...  " << Nsamples<< std::endl;
          return -1;
        }
        int countch=0;
        for (int i=0; i<NCH; i++) {
          if ((lchmask & (1<<i)) == (1<<i)) {
            channel = (unsigned int)i;
            board = nAdcBoard;
            savedata << "BRD_" << char( '0' + nAdcBoard ) << "," << "CH_" << char( '0' + countch ) << "," << biasval << ",";
            unsigned int samp1, samp2, samp3, samp4;
            for ( int wrd = 1; wrd < Nwords; wrd++ ){
              samp4 = (TotalData[board][ (4 + (countch * Nwords) + wrd) ] & 0xff000000UL) >> 24;
              samp3 = (TotalData[board][ (4 + (countch * Nwords) + wrd) ] & 0x00ff0000UL) >> 16;
              samp2 = (TotalData[board][ (4 + (countch * Nwords) + wrd) ] & 0x0000ff00UL) >>  8;
              samp1 = (TotalData[board][ (4 + (countch * Nwords) + wrd) ] & 0x000000ffUL)      ;
              savedata << samp1 << "," << samp2 << "," << samp3 << "," << samp4 << ",";
            }
            savedata << "\n";
            countch++;
          }
        }
      }else
      {
        std::cout << "Failed to read data: count = " << eventCount << std::endl;
      }
    }
    Event++;
  }


  // stop acquisition after successful read
  if (ADC_comm)
  {
    for (unsigned int count = 0; count < n_ADCs; count++)
    {
      adc[count]->Set_ACQ_RUN_STOP();
    }
  } 
  else 
  {
    std::cerr << std::endl << " ... acquisition stopped and communication lost. " << std::endl;
  }

  for (unsigned int count = 0; count < n_ADCs; count++) delete adc[count];
  return 0;
}
