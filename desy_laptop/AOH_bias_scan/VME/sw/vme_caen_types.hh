#ifndef CAEN_TYPES_
#define CAEN_TYPES_ 1

#define LINUX 1
#include "CAENVMElib.h"
#include "CAENVMEoslib.h"
#include "CAENVMEtypes.h"

typedef unsigned long UINT32;

typedef struct{
  int  m_vme_link;                /*!< \brief The link number */
  int  m_vme_board_num;           /*!< \brief The board number */
  UINT32 m_board_base_add;        /*!< \brief The board base address (32 bit) */
  char m_board_type[20];          /*!< \brief The board type */
} InputData;

typedef struct{
  int32_t m_vme_handle;           /*!< \brief  The VME library comunication handle */
} Handle;

#endif
