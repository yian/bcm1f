################################################################
# ADC Settings
################################################################
ADC_LINK=2                 # dec : ADC-PCI link (VME controller PCI card, starting from 0)
CHANNEL_MASK=0xff          # hex : channel mask (0xff default, fe = 11111110, 7f=01111111)

################################################################
# Data settings
################################################################
BUFFER_ORG=0x5 #a #5      # hex : resulting block size must be larger than 8*CUSTOM_SIZE (see manual 4.15.)!
    # 0xa=2kB, 0x9=4kB, 0x8=8kB, 0x7=16kB, 0x6=32kB, 0x5=64kB, 0x4=128kB, 0x3=256kB, 0x2=512kB, 0x1=1MB, 0x0=2MB
    # If CUSTOM_SIZE=0 (disabled) then BUFFER_ORG will determine the event size = block size * channels + header
CUSTOM_SIZE=5563 #  500 #5625 	   # dec : custom event size in "memory locations" (0 default; previous: 5625)
POST_TRIGGER_NUMBER=5500 #300 #5620    # dec : post trigger number in "memory locations" (minimum: 5; previous: 5620)
DATA_OUTPUT_PATHNAME=/brildata/bcm1futca/AOHscan/CshapeFULL_DETECTOR_BIAS_SWEEP_gain3_withPLT_-15C_noTP_HVcustomHV_noSource_2017-09-26_10-57/VME
DATA_OUTPUT_PATHNAME=/brildata/bcm1futca/AOHscan/CshapeFULL_DETECTOR_BIAS_SWEEP_gain3_withPLT_-15C_noTP_HVcustomHV_noSource_2017-09-26_10-57/VME
DATA_OUTPUT_PATHNAME=/brildata/bcm1futca/AOHscan/CshapeFULL_DETECTOR_BIAS_SWEEP_gain3_withPLT_-15C_noTP_HVcustomHV_noSource_2017-09-26_10-57/VME
#new BRM disk space since 09.2012
DATA_OUTPUT_PATHNAME=/brildata/bcm1futca/AOHscan/CshapeFULL_DETECTOR_BIAS_SWEEP_gain3_withPLT_-15C_noTP_HVcustomHV_noSource_2017-09-26_10-57/VME
DATA_OUTPUT_PATHNAME=/brildata/bcm1futca/AOHscan/CshapeFULL_DETECTOR_BIAS_SWEEP_gain3_withPLT_-15C_noTP_HVcustomHV_noSource_2017-09-26_10-57/VME
DATA_OUTPUT_PATHNAME=/brildata/bcm1futca/AOHscan/CshapeFULL_DETECTOR_BIAS_SWEEP_gain3_withPLT_-15C_noTP_HVcustomHV_noSource_2017-09-26_10-57/VME
#/data/bcm1f/ADC/    # string

################################################################
# Triggers and Selections -- all decimal
################################################################
# Note: all variables must be set even if unused (fixed number of input parameters!).
TRIGGER_MODE=0
		  # 0=external trigger;    (1=SW trigger);    2=absolute threshold;
			  # 3=ADC counts relative to baseline;   4=sigmas relative to baseline.

TRG_THRES_CH0=2 	  # floating point :
TRG_THRES_CH1=2 	  # Thresholds for self-triggering or filtering, respectively, as follows:
TRG_THRES_CH2=2 	  # TRIGGER_MODE=0: Flat lines (below the *relative* thresholds) are filtered out.
TRG_THRES_CH3=2 	  # TRIGGER_MODE=1: no obvious use of thresholds yet.
TRG_THRES_CH4=2 	  # TRIGGER_MODE=2: self-trigger thresholds at the given ADC levels.
TRG_THRES_CH5=2 	  # TRIGGER_MODE=3: self-trigger thresholds in ADC counts w.r.t. the baselines.
TRG_THRES_CH6=4 	  # TRIGGER_MODE=4: self-trigger thresholds in std. deviations w.r.t. the baselines.
TRG_THRES_CH7=2 

TIME_OU_THRES_CH0=4	  # integer : minimum time over/under threshold
TIME_OU_THRES_CH1=4
TIME_OU_THRES_CH2=4
TIME_OU_THRES_CH3=4
TIME_OU_THRES_CH4=4
TIME_OU_THRES_CH5=4
TIME_OU_THRES_CH6=4
TIME_OU_THRES_CH7=4

N_ADCS=6
#BASEADDRESS[0]=0x88880000    # TIF, SN 768, hex : VME module's base address
#BASEADDRESS[1]=0x22220000    # TIF, SN 520,  -- with pulse in ch 0

BASEADDRESS[0]=0x55550000    # p5, SN 780, link0 
BASEADDRESS[1]=0x44440000    # p5, SN 468, link1
BASEADDRESS[2]=0x33330000    # p5, SN 109, link2
BASEADDRESS[3]=0x11110000    # p5, SN 152, link3
BASEADDRESS[4]=0x66660000    # p5, SN 317, link4
BASEADDRESS[5]=0x77770000    # p5, SN 862, link5
