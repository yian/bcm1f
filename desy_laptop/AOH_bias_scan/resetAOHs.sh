#!/bin/bash

source /etc/init.d/functions

# Reset to default values after scan
echo "Returning slow control to default state (custom bias settings), please wait..."
export LD_LIBRARY_PATH=/opt/cactus/lib:/brilpro/bcm1f/Slow_Control/lib
bash /brilpro/bcm1f/Slow_Control/configureAllAOHsWithSeparateValues.sh
echo "Slow control returned to default state."

###
echo "DONE!"
