#!/bin/sh

# export LD_LIBRARY_PATH=/opt/cactus/lib:$LD_LIBRARY_PATH
# for VME:
export LD_LIBRARY_PATH=/opt/cactus/lib:/brilpro/bcm1f/Slow_Control/lib:/opt/xdaq/lib/:/brilpro/bcm1f/software_adc/lib/:/brilpro/bcm1f/slow_hub_controller/lib/:/brilpro/bcm1f/CshapeTesting/bcm1f_slow_control_SVN/lib/:/opt/root/hcal-root-5.34.36-gcc485/lib
export PATH=/opt/cactus/bin/:$PATH
export ROOTSYS=/nfshome0/lumipro/brilconda/root
export PATH=$ROOTSYS/bin:$PATH
# export LD_LIBRARY_PATH=$ROOTSYS/lib:/nfshome0/lumipro/brilconda/lib
export PYTHONPATH=.
export PYTHONPATH=/nfshome0/lumipro/brilconda/lib
export PYTHONPATH=$ROOTSYS/lib:$PYTHONPATH
export PYTHONPATH=${PWD}:$PYTHONPATH
# export PYTHONPATH=${PWD}/fits:$PYTHONPATH
# export PYTHONPATH=${PWD}/corrections:$PYTHONPATH
# export PYTHONPATH=${PWD}/Automation:$PYTHONPATH
export VDMPATH=$PWD
export PATH=/nfshome0/lumipro/brilconda/bin:$PATH
unset ROOTSYS
