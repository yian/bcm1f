#!/bin/bash

usage() {
    echo 'Usage: ./analyse_VME.sh <SUPER_DIRECTORY> <VME|uTCA|ALL>'
}

if test $# -ne 2
then
    echo 'Wrong number of arguments in analyse_VME.sh!'
    usage 
    exit 1
else
    SUPER_DIRECTORY=$1
    SYSTEM=$2
fi

# sanity check
if [[ "${SYSTEM}" = "VME" ]] || [[ "${SYSTEM}" = "uTCA" ]] || [[ "${SYSTEM}" = "ALL" ]]
then
    echo "Analyse data for: $SYSTEM"
else
    usage
    #exit 1
fi

if [[ "${SYSTEM}" = "VME" ]]
then
    PATH_TO_FILE=/brildata/bcm1futca/AOHscan/${SUPER_DIRECTORY}/${SYSTEM}/
    python ./VME/scripts/analyse_VME.py ${PATH_TO_FILE}
    convert "./*.{png,}" results/${SUPER_DIRECTORY}.pdf
    rm ./*.png
    mv recommendationVME.csv results/${SUPER_DIRECTORY}.csv
    echo "DONE!"
elif [[ "${SYSTEM}" = "uTCA" ]]
then
    echo "uTCA not implemented yet..."
    echo "STOP"
elif [[ "${SYSTEM}" = "ALL" ]]
then
    echo "uTCA not implemented yet..."
    echo "STOP"
fi
