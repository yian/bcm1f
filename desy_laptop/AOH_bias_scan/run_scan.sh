#!/bin/bash
# trap "" HUP

source /etc/init.d/functions

. env.sh

#######################################
# VARIABLES
#######################################

GAIN=3

#######################################
# FUNCTIONS
#######################################

# cute timer function from http://www.linuxjournal.com/content/use-date-command-measure-elapsed-time
timer()
{
    if [[ $# -eq 0 ]]; then
        echo $(date '+%s')
    else
        local  stime=$1
        etime=$(date '+%s')
        if [[ -z "$stime" ]]; then stime=$etime; fi
        dt=$((etime - stime))
        ds=$((dt % 60))
        dm=$(((dt / 60) % 60))
        dh=$((dt / 3600))
        printf '%d:%02d:%02d' $dh $dm $ds
    fi
}

usage() {
    echo 'Usage: ./run_scan.sh <VME|uTCA|ALL> <bias_start [DEC]> <bias_stop [DEC] (max 63)>'
}

#######################################
# GET INPUT ARGUMENTS
#######################################

if  test $# -ne 3
then
    echo 'Wrong number of arguments!'
    usage
    exit 1
else
    SYSTEMS=$1
    START=$2
    STOP=$3
fi

START_TIME=$(timer)

#######################################
# SANITY-CHECK INPUT ARGUMENTS
#######################################

if [[ "${SYSTEMS}" = "VME" ]] || [[ "${SYSTEMS}" = "uTCA" ]] || [[ "${SYSTEMS}" = "ALL" ]]
then
    echo "Systems to perform the scan: $SYSTEMS"
else
    usage
    #exit 1
fi

#######################################
# SETUP DIRECTORIES
#######################################

DATE=$(date +%Y-%m-%d_%H-%M)

SUPER_DIRECTORY=${DATE}_gain${GAIN}_systems${SYSTEMS}
SUPER_DIRECTORY_FULL_PATH=/brildata/bcm1futca/AOHscan/${SUPER_DIRECTORY} ###########################
# SUPER_DIRECTORY_FULL_PATH=~/AOH_bias_scan/${SUPER_DIRECTORY}
mkdir ${SUPER_DIRECTORY_FULL_PATH}

if [[ "${SYSTEMS}" = "VME" ]]
then
    mkdir ${SUPER_DIRECTORY_FULL_PATH}/VME
    # exit
    # ssh brilvme4
elif [[ "${SYSTEMS}" = "uTCA" ]]
then
    mkdir ${SUPER_DIRECTORY_FULL_PATH}/uTCA
    # exit
    # ssh brilutca2
else
    mkdir ${SUPER_DIRECTORY_FULL_PATH}/VME
    mkdir ${SUPER_DIRECTORY_FULL_PATH}/uTCA
fi

# mkdir ./Results/${SUPER_DIRECTORY}


#######################################
# LOOPING OVER BIAS VALUES
#######################################
DIR="$(pwd)"
for (( BIAS = $START; BIAS <= $STOP; BIAS++ ))
do
    printf -v BIAS_HEX '%02x' ${BIAS}
    printf -v BIAS_DEC '%02d' ${BIAS}
    BIAS_HEX="0x${BIAS_HEX}"
    echo "Starting bias value ${BIAS_HEX}"
    
    cd /brilpro/bcm1f/CshapeTesting/bcm1f_slow_control_SVN/
    for PORT in 0 1 2 3
    do
        for LASER in L M R
        do
            /brilpro/bcm1f/CshapeTesting/bcm1f_slow_control_SVN/bin/slow_control_direct_choice AOH ${PORT} ${LASER} ${BIAS_HEX} ${GAIN} ALL quiet
        done
    done
    cd -
    
    if [[ "${SYSTEMS}" = "VME" ]]
    then
        # export LD_LIBRARY_PATH=/opt/cactus/lib:/brilpro/bcm1f/Slow_Control/lib:/opt/xdaq/lib/:/brilpro/bcm1f/software_adc/lib/:/brilpro/bcm1f/slow_hub_controller/lib/:/brilpro/bcm1f/CshapeTesting/bcm1f_slow_control_SVN/lib/:/opt/root/hcal-root-5.34.36-gcc485/lib
        bash $DIR/VME/run_VME.sh ${BIAS_DEC} ${SUPER_DIRECTORY_FULL_PATH}
    elif [[ "${SYSTEMS}" = "uTCA" ]]
    then
        export LD_LIBRARY_PATH=/nfshome0/aruede/sw/lib/:/opt/cactus/lib/:/brilpro/bcm1f/software_adc/lib/:/brilpro/bcm1f/slow_hub_controller/lib/:/brilpro/bcm1f/CshapeTesting/bcm1f_slow_control_SVN/lib:/opt/cactus/lib:/brilpro/bcm1f/Slow_Control/lib:/opt/xdaq/lib/
        bash $DIR/uTCA/run_uTCA.sh ${BIAS_DEC} ${SUPER_DIRECTORY_FULL_PATH}
    else
        # exit
        # ssh brilvme4
        export LD_LIBRARY_PATH=/opt/cactus/lib:/brilpro/bcm1f/Slow_Control/lib:/opt/xdaq/lib/:/brilpro/bcm1f/software_adc/lib/:/brilpro/bcm1f/slow_hub_controller/lib/:/brilpro/bcm1f/CshapeTesting/bcm1f_slow_control_SVN/lib/:/opt/root/hcal-root-5.34.36-gcc485/lib
        bash $DIR/VME/run_VME.sh ${BIAS_DEC} ${SUPER_DIRECTORY_FULL_PATH}
        # exit
        # ssh brilutca2
        export LD_LIBRARY_PATH=/nfshome0/aruede/sw/lib/:/opt/cactus/lib/:/brilpro/bcm1f/software_adc/lib/:/brilpro/bcm1f/slow_hub_controller/lib/:/brilpro/bcm1f/CshapeTesting/bcm1f_slow_control_SVN/lib:/opt/cactus/lib:/brilpro/bcm1f/Slow_Control/lib:/opt/xdaq/lib/
        bash $DIR/uTCA/run_uTCA.sh ${BIAS_DEC} ${SUPER_DIRECTORY_FULL_PATH}
    fi
done

# Reset to default values after scan
bash resetAOHs.sh
#touch reset_success.txt

echo "SCAN_NAME: ${SUPER_DIRECTORY}"
echo "DONE!"
printf 'Total elapsed time: %s\n' $(timer ${START_TIME})
# trap - HUP
